
from django.conf import settings
from django.urls import include, path, re_path, reverse_lazy
from django.conf.urls.static import static
from django.views.generic import RedirectView

from django.contrib import admin


urlpatterns = [
    # path('accounts/', include('accounts.urls')),
    path('admin/', admin.site.urls),
    path('', RedirectView.as_view(url=reverse_lazy("shipment")), name="home"),
    path('seeds/', include('seeds.urls')),
    path('', include('django.contrib.auth.urls'))
]


if settings.DEBUG:
    from django.conf.urls import include
    import debug_toolbar
    urlpatterns.append(re_path(r'^__debug__/', include(debug_toolbar.urls)))

    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)