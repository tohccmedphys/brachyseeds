
from django.conf import settings
from random import Random


def site(request):

    return {
        'VERSION': settings.VERSION,
        "MAX_CACHE_TIMEOUT": settings.MAX_CACHE_TIMEOUT,
        "FORCE_SCRIPT_NAME": settings.FORCE_SCRIPT_NAME,
        'CSS_VERSION': Random().randint(1, 1000) if settings.DEBUG else settings.VERSION,
        'MIN': '' if settings.DEBUG else '.min',
        'DEBUG': settings.DEBUG,
        'CSRF_COOKIE_NAME': settings.CSRF_COOKIE_NAME,

        'PYTHON_DATE_FORMAT': settings.PYTHON_DATE_FORMAT,
        'FRONTEND_DATE_DISPLAY_FORMAT': settings.FRONTEND_DATE_DISPLAY_FORMAT,
        'FRONTEND_DATE_DATA_FORMAT': settings.FRONTEND_DATE_DATA_FORMAT,
        'FRONTEND_TIME_DATA_FORMAT': settings.FRONTEND_TIME_DATA_FORMAT,
        'FRONTEND_DATE_DATA_FORMAT_MOMENT': settings.FRONTEND_DATE_DATA_FORMAT_MOMENT,
        'FRONTEND_TIME_DATA_FORMAT_MOMENT': settings.FRONTEND_TIME_DATA_FORMAT_MOMENT,
        'FRONTEND_DATE_DISPLAY_FORMAT_MOMENT': settings.FRONTEND_DATE_DISPLAY_FORMAT_MOMENT,
        'FRONTEND_DATETIME_DATA_FORMAT_MOMENT': settings.FRONTEND_DATETIME_DATA_FORMAT_MOMENT,
    }
