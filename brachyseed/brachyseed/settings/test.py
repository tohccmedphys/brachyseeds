from base import *

########## TEST SETTINGS
TEST_DISCOVER_TOP_LEVEL = SITE_ROOT
TEST_DISCOVER_ROOT = SITE_ROOT
TEST_DISCOVER_PATTERN = "test_*.py"

########## IN-MEMORY TEST DATABASE
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": ":memory:",
        "USER": "",
        "PASSWORD": "",
        "HOST": "",
        "PORT": "",
    },
}

########## TEST CONFIGURATION
# TEST_RUNNER = 'django_coverage.coverage_runner.CoverageRunner'
COVERAGE_ADDITIONAL_MODULES = ["custom_form.tests"]
# COVERAGE_MODULE_EXCLUDES = [
#    'settings$',  'locale$',
#    'django', 'migrations'
#]