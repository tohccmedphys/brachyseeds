"""Production settings and globals."""


from base import *

#database_settings contains database user/password information that should not go in version control!
from database_settings import *

#active_directory_settings contains database user/password information that should not go in version control!
from active_directory_settings import *
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'accounts.backends.ActiveDirectoryGroupMembershipSSLBackend',
)

#email_settings contains database user/password information that should not go in version control!
from email_settings import *


DEFAULT_GROUP_NAMES = []
FORCE_SCRIPT_NAME = "/brachyseeds"
LOGIN_REDIRECT_URL = "/brachyseeds"
LOGIN_URL = "/brachyseeds/login"
STATIC_URL = '/brachyseeds_static/'
DEBUG = True


ADMINS = (
    ('Randle Taylor', 'rataylor@toh.on.ca'),
    ('Randle Taylor', 'randle.taylor@gmail.com'),
)
MANAGERS = ADMINS

from database_settings import *
