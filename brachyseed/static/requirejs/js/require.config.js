
console.log(SiteConfig)

require.config({
   urlArgs: (function () {
        if (SiteConfig.DEBUG === 'True') {
            return 'v=' + Math.random();
        }
        return 'v=' + SiteConfig.VERSION;
    }()),
    baseUrl: SiteConfig.STATIC_URL,
    paths: {
        // Third party:
        adminlte: SiteConfig.STATIC_URL + 'adminlte/js/adminlte' + SiteConfig.MIN,
        adminlte_config: SiteConfig.STATIC_URL + 'adminlte/js/adminlte.config',
        daterangepicker: SiteConfig.STATIC_URL + 'daterangepicker/js/daterangepicker',
        // datepicker: SiteConfig.STATIC_URL + 'flatpickr/js/flatpickr',
        flatpickr: SiteConfig.STATIC_URL + 'flatpickr/js/flatpickr',
        jquery: SiteConfig.STATIC_URL + 'jquery/js/jquery' + SiteConfig.MIN,
        knockout: SiteConfig.STATIC_URL + 'js/knockout',
        bootstrap: SiteConfig.STATIC_URL + 'bootstrap/js/bootstrap.bundle' + SiteConfig.MIN,
        'bootstrap-multiselect': SiteConfig.STATIC_URL + 'multiselect/js/bootstrap-multiselect',
        // 'bootstrap.datepicker': SiteConfig.STATIC_URL + 'datepicker/js/bootstrap-datepicker',
        // 'bootstrap.editable': SiteConfig.STATIC_URL + 'bootstrap-editable/js/bootstrap-editable',
        'bootstrap-colorpicker': SiteConfig.STATIC_URL + 'bootstrap-colorpicker/js/bootstrap-colorpicker' + SiteConfig.MIN,
        d3: SiteConfig.STATIC_URL + 'd3/js/d3' + SiteConfig.MIN,
        lodash: SiteConfig.STATIC_URL + 'lodash/js/lodash',
        json2: SiteConfig.STATIC_URL + 'json2/js/json2',
        // django: SiteConfig.STATIC_URL + 'js/djangojs/django',
        moment: SiteConfig.STATIC_URL + 'moment/js/moment',
        ajaxcomments: SiteConfig.STATIC_URL + 'js/ajaxcomments',
        selectize: SiteConfig.STATIC_URL + 'selectize/js/selectize',
        datatables: SiteConfig.STATIC_URL + 'listable/js/jquery.dataTables' + SiteConfig.MIN,
        'datatables.bootstrap': SiteConfig.STATIC_URL + 'listable/js/jquery.dataTables.bootstrap',
        'datatables.columnFilter': SiteConfig.STATIC_URL + 'listable/js/jquery.dataTables.columnFilter',
        'datatables.searchPlugins': SiteConfig.STATIC_URL + 'listable/js/jquery.dataTables.searchPlugins',
        'datatables.sort': SiteConfig.STATIC_URL + 'listable/js/jquery.dataTables.sort',
        listable: SiteConfig.STATIC_URL + 'listable/js/listable',
        multiselect: SiteConfig.STATIC_URL + 'multiselect/js/bootstrap.multiselect',
        ko: SiteConfig.STATIC_URL + 'ko/js/ko',

        fontawesome: SiteConfig.STATIC_URL + 'fontawesome/js/fontawesome' + SiteConfig.MIN,
        popper: SiteConfig.STATIC_URL + 'popper/js/popper',
        select2: SiteConfig.STATIC_URL + 'select2/js/select2.full' + SiteConfig.MIN,
        toastr: SiteConfig.STATIC_URL + 'toastr/js/toastr' + SiteConfig.MIN,

        // d3: SiteConfig.STATIC_URL + 'd3/js/'

        // Site wide:
        project: SiteConfig.STATIC_URL + 'brachyseed/js/project',
        
        //Seeds:
        shipment: SiteConfig.STATIC_URL + 'seeds/js/shipment',
        shipment_detail: SiteConfig.STATIC_URL + 'seeds/js/shipment_detail',
        empty_vial: SiteConfig.STATIC_URL + 'seeds/js/empty_vial',
        stats: SiteConfig.STATIC_URL + 'seeds/js/stats'
    },
    shim: {
        adminlte: {
            deps: ['jquery', 'bootstrap', 'adminlte_config']
        },
        bootstrap: {
            deps: ['jquery']
        },
        datatables: {
            deps: ['jquery', 'bootstrap'],
            exports: 'dataTable'
        },
        'datatables.bootstrap': {
            deps: ['datatables']
        },
        'datatables.columnFilter': {
            deps: ['datatables']
        },
        'datatables.searchPlugins': {
            deps: ['datatables']
        },
        'datatables.sort': {
            deps: ['datatables']
        },
        // datepicker: {
        //     deps: ['jquery', 'bootstrap']
        // },
        jquery: {
            exports: '$'
        },
        ko: {
            deps: ['jquery'],
            exports: 'ko'
        },
        listable: {
            deps: ['jquery', 'datatables', 'datatables.columnFilter', 'datatables.searchPlugins', 'datatables.sort', 'datatables.bootstrap', 'multiselect', 'daterangepicker']
        },
        lodash: {
            exports: '_'
        },
        project: {
            deps: ['jquery', 'adminlte']
        }
    }
});
