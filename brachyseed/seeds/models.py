
import math

from collections import OrderedDict
from django.conf import settings
from django.contrib import auth
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe


def mCi2MBq(mCi):
    return 37. * mCi


def MBq2mCi(mBq):
    return mBq / 37.


LN2 = math.log(2)

I125 = "I125"
PD103 = "Pd103"
CS137 = "Cs137"
CS137_T12 = 11018.3
CS137_DECAY_CONST = LN2/CS137_T12


ISOTOPES = OrderedDict([
    (I125, {
        "name": I125,
        "half_life": 59.40,
        "air_kerma_conversion_factor": 1.270 / mCi2MBq(1.),  # [U/mCi]/[MBq/mCi]
    }),
    (PD103, {
        "name": PD103,
        "half_life": 16.991,
        "air_kerma_conversion_factor": 1.293 / mCi2MBq(1.),  # [U/mCi]/[MBq/mCi]
    }),

])

ISOTOPE_CHOICES = [(i["name"], i["name"]) for i in ISOTOPES.values()]


class Shipment(models.Model):

    po_number = models.CharField(max_length=255)
    received = models.DateField(verbose_name="Date Shipment Received")
    received_by = models.ForeignKey(User, on_delete=models.PROTECT)
    reference_date = models.DateField(verbose_name="Activity Reference Date")
    activity_per_seed = models.FloatField(verbose_name="Activity Per Seed (MBq)", null=True, blank=True)
    background = models.FloatField(verbose_name="Background reading (&micro;R/h)", default=settings.BACKGROUND_RADIATION_DEFAULT)
    surface = models.FloatField(verbose_name="Surface reading (&micro;R/h)")
    dose_rate_1m = models.FloatField(verbose_name="Dose rate at 1m (&micro;R/h)")

    class Meta:
        ordering = ["-received"]

    def cur_activity_per_seed(self):
        t12 = ISOTOPES[self.isotope()]["half_life"]
        decay_const = LN2 / t12
        days = (timezone.now().date() - self.reference_date).days
        return self.activity_per_seed * math.exp(-decay_const * days)

    def isotope(self):
        return self.shipmentitem_set.select_related("implant_type")[0].implant_type.isotope

    def number_of_seeds(self):
        items = self.shipmentitem_set.select_related("implant_type").all()
        return sum([i.total_seeds() for i in items])

    def activity_received(self):
        return self.number_of_seeds() * self.activity_per_seed

    def activity_per_seed_mci(self):
        return MBq2mCi(self.activity_per_seed)

    def latest_calibration(self):
        try:
            return self.calibration_set.latest("measured")
        except Calibration.DoesNotExist:
            pass

    def surface_exceeded(self):
        return self.surface > settings.SURFACE_READING_LIMIT

    def dose_rate_1m_exceeded(self):
        return self.dose_rate_1m > settings.DOSE_RATE_1M_LIMIT

    def __str__(self):
        return self.po_number

    def model_to_dict(self):
        return {
            'id': self.id,
            'po_number': self.po_number,
            'received': self.received.strftime(settings.BACKEND_DATE_DATA_FORMAT),
            'received_by': self.received_by.username,
            'reference_date': self.reference_date.strftime(settings.BACKEND_DATE_DATA_FORMAT),
            'activity_per_seed': self.activity_per_seed,
            'background': self.background,
            'surface': self.surface,
            'dose_rate_1m': self.dose_rate_1m
        }


class ImplantType(models.Model):

    isotope = models.CharField(max_length=5, choices=ISOTOPE_CHOICES)
    name = models.CharField(max_length=255)
    seeds_per_implant = models.PositiveIntegerField()

    class Meta:
        unique_together = (("isotope", "name",),)

    def __str__(self):
        return "{} : {}".format(self.name, ISOTOPES[str(self.isotope)]["name"])

    def model_to_dict(self):
        return {
            'id': self.id,
            'isotope': self.isotope,
            'name': self.name,
            'seeds_per_implant': self.seeds_per_implant,
        }


class ShipmentItem(models.Model):

    shipment = models.ForeignKey(Shipment, models.CASCADE)
    implant_type = models.ForeignKey(ImplantType, models.PROTECT)
    number = models.PositiveIntegerField(help_text="Number of this seed type received")

    class Meta:
        unique_together = (("shipment", "implant_type",),)

    def total_seeds(self):
        return self.implant_type.seeds_per_implant * self.number

    def orig_activity_per_seed(self):
        return self.shipment.activity_per_seed

    def orig_activity_total(self):
        return self.total_seeds() * self.orig_activity_per_seed()

    def cur_activity_per_seed(self):
        Ao = self.orig_activity_per_seed()
        t12 = ISOTOPES[self.implant_type.isotope]["half_life"]
        decay_const = LN2 / t12
        days = (timezone.now().date() - self.shipment.reference_date).days
        return Ao * math.exp(-decay_const * days)

    def cur_activity_total(self):
        return self.total_seeds() * self.cur_activity_per_seed()

    def __str__(self):
        return "Item %d from Shipment %d" % (self.pk, self.shipment_id)

    def model_to_dict(self):
        return {
            'id': self.id,
            'shipment': self.shipment_id,
            'implant_type': self.implant_type_id,
            'number': self.number
        }


class Calibration(models.Model):

    shipment = models.ForeignKey(Shipment, on_delete=models.CASCADE)

    batch_number = models.CharField(max_length=255, null=True, blank=True)
    well_reading = models.FloatField(verbose_name="Well counter check reading")
    well_reading_ref = models.FloatField(verbose_name="Well counter check reference")

    measured = models.DateField(verbose_name="Date Measured")
    measured_by = models.ForeignKey(User, related_name="calibrationmeasure_user", on_delete=models.PROTECT)

    reviewed = models.DateField(verbose_name="Date Reviewed", null=True, blank=True)
    reviewed_by = models.ForeignKey(User, related_name="calibrationreview_user", null=True, blank=True, on_delete=models.PROTECT)

    def model_to_dict(self):
        return {
            'id': self.id,
            'shipment': self.shipment_id,
            'batch_number': self.batch_number if self.batch_number else '',
            'well_reading': self.well_reading,
            'well_reading_ref': self.well_reading_ref,
            'measured': self.measured.strftime(settings.BACKEND_DATE_DATA_FORMAT),
            'measured_by': self.measured_by.username,
            'reviewed': self.reviewed.strftime(settings.BACKEND_DATE_DATA_FORMAT) if self.reviewed else '',
            'reviewed_by': self.reviewed_by.username if self.reviewed_by else '',
            'measurements': [m.model_to_dict() for m in self.calibrationmeasurement_set.all()]
        }


class CalibrationMeasurement(models.Model):

    calibration = models.ForeignKey(Calibration, on_delete=models.CASCADE)
    number_seeds = models.PositiveIntegerField(verbose_name="Number of Seeds Measured")
    reading = models.FloatField(verbose_name="Calibrator Reading (MBq)")
    measured_kerma = models.FloatField(verbose_name="Measured Air Kerma Per Seed (Gy m2 h^-1)")
    expected_kerma = models.FloatField(verbose_name="Expected Air Kerma Per Seed (Gy m2 h^-1)")
    error = models.FloatField(verbose_name="% Error")

    def model_to_dict(self):
        return {
            'id': self.id,
            'calibration': self.calibration_id,
            'number_seeds': self.number_seeds,
            'reading': self.reading,
            'measured_kerma': self.measured_kerma,
            'expected_kerma': self.expected_kerma,
            'error': self.error
        }


class StorageVial(models.Model):

    vial_id = models.CharField(max_length=255, unique=True, verbose_name="Unique Storage Vial ID")
    last_emptied = models.DateField(null=True, blank=True)

    def cur_activity(self):

        shipment_isotopes = dict(Shipment.objects.values_list("pk", "shipmentitem__implant_type__isotope"))
        qs = self.storagevialseeds_set.filter(disposed=False).select_related(
            "shipment",
        )
        cur_activity = 0
        for svs in qs:

            Ao = svs.shipment.activity_per_seed
            isotope = shipment_isotopes[svs.shipment.pk]
            t12 = ISOTOPES[isotope]["half_life"]
            decay_const = LN2 / t12
            days = (timezone.now().date() - svs.shipment.reference_date).days
            cur_activity += svs.number_seeds*(Ao * math.exp(-decay_const * days))
        return cur_activity

    def cur_number_seeds(self):
        qs = self.storagevialseeds_set.filter(disposed=False)
        return sum([seeds.number_seeds for seeds in qs])

    def empty(self):
        self.storagevialseeds_set.update(disposed=True)
        self.last_emptied = timezone.now().date()
        self.save()

    def __str__(self):
        return self.vial_id

    def model_to_dict(self):
        return {
            'id': self.id,
            'vial_id': self.vial_id,
            'last_emptied': self.last_emptied.strftime(settings.BACKEND_DATE_DATA_FORMAT) if self.last_emptied else '',
        }


class RadOnc(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name

    def model_to_dict(self):
        return {
            'id': self.id,
            'name': self.name
        }


class PatientImplant(models.Model):

    shipment = models.ForeignKey(Shipment, on_delete=models.PROTECT)
    patient_id = models.CharField(max_length=255)
    implanted = models.DateField(verbose_name="Patient Implant Date")
    number_seeds = models.PositiveIntegerField(verbose_name="Total Number of Seeds Implanted")
    radonc = models.ForeignKey(RadOnc, verbose_name="Radiation Oncologist", on_delete=models.PROTECT)

    def model_to_dict(self):
        return {
            'id': self.id,
            'shipment': self.shipment_id,
            'patient_id': self.patient_id,
            'implanted': self.implanted.strftime(settings.BACKEND_DATE_DATA_FORMAT),
            'number_seeds': self.number_seeds,
            'radonc': self.radonc_id,
            'stored_seeds': [s.model_to_dict() for s in self.storagevialseeds_set.all()]
        }


class StorageVialSeedManager(models.Manager):

    def current_seed_stats(self):

        qs = self.get_queryset().filter(disposed=False).select_related(
            "shipment",
            # "shipment__shipmentitem_set__shipment",
            # "shipment__shipmentitem_set__implant_type",
        )
        #     .prefetch_related(
        #     "shipment__shipmentitem_set__implant_type"
        # )

        shipment_isotopes = dict(Shipment.objects.values_list("pk", "shipmentitem__implant_type__isotope"))
        nseeds = sum([svi.number_seeds for svi in qs])

        current_activity = 0

        for svi in qs:

            Ao = svi.shipment.activity_per_seed
            isotope = shipment_isotopes[svi.shipment.pk]
            t12 = ISOTOPES[isotope]["half_life"]
            decay_const = LN2 / t12
            days = (timezone.now().date() - svi.shipment.reference_date).days
            current_activity += svi.number_seeds*(Ao * math.exp(-decay_const * days))


        return {
            "total_seeds": nseeds,
            "current_activity": current_activity,
        }


class StorageVialSeeds(models.Model):

    shipment = models.ForeignKey(Shipment, on_delete=models.PROTECT)
    patient_implant = models.ForeignKey(PatientImplant, on_delete=models.PROTECT)
    number_seeds = models.PositiveIntegerField(help_text="Total number of seeds stored")
    date_stored = models.DateField()
    storage_vial = models.ForeignKey(StorageVial, on_delete=models.PROTECT)
    disposed = models.BooleanField(default=False)

    objects = StorageVialSeedManager()

    class Meta:
        verbose_name_plural = "Storage Vial Seeds"

    def cur_activity_per_seed(self):
        return self.shipment.cur_activity_per_seed()

    def cur_activity_total(self):

        return self.number_seeds * self.cur_activity_per_seed()

    def model_to_dict(self):
        return {
            'id': self.id,
            'shipment': self.shipment_id,
            'patient_implant': self.patient_implant_id,
            'number_seeds': self.number_seeds,
            'date_stored': self.date_stored.strftime(settings.BACKEND_DATE_DATA_FORMAT),
            'storage_vial': self.storage_vial_id,
            'disposed': self.disposed
        }
