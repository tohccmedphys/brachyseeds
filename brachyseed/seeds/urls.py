
from django.urls import include, path, re_path, reverse_lazy
from seeds import views


urlpatterns = [
    path('shipment/', views.ShipmentList.as_view(), name="shipment-list"),
    path('shipment/new/', views.Shipment.as_view(), name="shipment"),
    re_path(r'^shipment/(?P<pk>\d+)/$', views.ShipmentDetail.as_view(), name="shipment-detail"),
    path('stats/', views.Stats.as_view(), name="stats"),
    path('get_stats_data/', views.GetStatsData.as_view(), name='get_stats_data'),
    re_path(r'^storage/(?P<pk>\d+)/empty/$', views.EmptyStorage.as_view(), name="empty-storage"),
]
