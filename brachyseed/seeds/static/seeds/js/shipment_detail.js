
require(['jquery', 'lodash', 'moment', 'ko', 'flatpickr', 'toastr', 'select2'], function($, _, moment, ko, Flatpickr, toastr) {

    let csrftoken;

    let flatpickr_options = {
        dateFormat: SiteConfig.FRONTEND_DATE_DATA_FORMAT,
        altFormat: SiteConfig.FRONTEND_DATE_DISPLAY_FORMAT,
        altInput: true,
        allowInput: true
        // wrap: true
    };

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "3000",
        "extendedTimeOut": "5000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        // "tapToDismiss": true
    };

    ko.bindingHandlers.select2 = {

        init: function(el, valueAccessor, allBindingsAccessor, viewModel) {
            ko.utils.domNodeDisposal.addDisposeCallback(el, function () {
                $(el).select2('destroy');
            });

            const allBindings = allBindingsAccessor(),
                select2 = ko.utils.unwrapObservable(allBindings.select2);

            $(el).select2(select2);
        },
        update: function (el, valueAccessor, allBindingsAccessor, viewModel) {
            const allBindings = allBindingsAccessor();

            if ("value" in allBindings) {
                if ((allBindings.select2.multiple || el.multiple) && allBindings.value().constructor != Array) {
                    $(el).val(allBindings.value().split(',')).trigger('change');
                }
                else {
                    $(el).val(allBindings.value()).trigger('change');
                }
            } else if ("selectedOptions" in allBindings) {
                const converted = [];
                let textAccessor = function (value) {
                    return value;
                };
                if ("optionsText" in allBindings) {
                    textAccessor = function (value) {
                        let valueAccessor = function (item) {
                            return item;
                        };
                        if ("optionsValue" in allBindings) {
                            valueAccessor = function (item) {
                                return item[allBindings.optionsValue];
                            }
                        }
                        const items = $.grep(allBindings.options(), function (e) {
                            return valueAccessor(e) == value
                        });
                        if (items.length == 0 || items.length > 1) {
                            return "UNKNOWN";
                        }
                        return items[0][allBindings.optionsText];
                    }
                }
                $.each(allBindings.selectedOptions(), function (key, value) {
                    converted.push({id: value, text: textAccessor(value)});
                });
                $(el).select2("data", converted);
            }
            $(el).trigger("change");
        }
    };

    ko.bindingHandlers.flatpickr = {
        init: function (element, valueAccessor, allBindingsAccessor) {
            const options = $.extend(flatpickr_options, allBindingsAccessor().flatpickrOptions);
            const $el = $(element);
            let picker;

            if (options.wrap) {
                picker = new Flatpickr(element.parentNode, options);
            } else {
                picker = new Flatpickr(element, options);
            }

            // Save instance for update method.
            $el.data('datetimepickr_inst', picker);

            // handle the field changing by registering datepicker's changeDate event
            ko.utils.registerEventHandler(element, "change", function () {
                valueAccessor()(picker.parseDate($el.val()));
            });

            // handle disposal (if KO removes by the template binding)
            ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
                $el.flatpickr("destroy");
            });
        },
        update: function (element, valueAccessor, allBindingsAccessor) {
            // Get datepickr instance.
            const picker = $(element).data('datetimepickr_inst');
            const value = ko.unwrap(valueAccessor());

            picker.setDate(value);
        }
    };

    const MAX_INDIVIDUAL_ERR = 10;
    const MAX_AVG_ERR = 5;
    const MAX_CAL_MEASUREMENTS = 6;
    const DETECTOR_CAL_FACTOR = 1.15;
    const DATE_FORMAT = "YYYY-MM-DD";


    function implantTypeForItem(item) {
        return _.find(ImplantTypeData, function (it) {
            return it.id === item.implant_type
        });
    }

    function ms2days(ms) {
        return ms / (1000 * 60 * 60 * 24);
    }

    function Shipment(data) {
        const self = this;
        _.extend(self, data);
    }

    function Calibration(data) {

        const self = this;

        self.cal_id = data.cal_id;
        self.id = ko.observable(data.id || null);
        self.batchNumber = ko.observable(data.batch_number || null);
        self.wellReading = ko.observable(data.well_reading || null);
        self.wellReadingRef = ko.observable(data.well_reading_ref || wellRef);
        self.wellReadingTol = ko.observable(data.well_reading_tol || wellTol);

        self.calibrationDate = ko.observable(data.measured || null);
        self.calibrationDateDisplay = ko.computed(function() {
            return self.calibrationDate() ? moment(self.calibrationDate(), SiteConfig.FRONTEND_DATE_DATA_FORMAT_MOMENT).format(SiteConfig.FRONTEND_DATE_DISPLAY_FORMAT_MOMENT) : null;
        });
        self.measurements = ko.observableArray([]);

        self.reviewers = ko.observableArray(reviewers);

        self.reviewedBy = ko.observable(data.reviewed_by || null);
        self.username = ko.observable("");
        self.password = ko.observable("");

        self.minDate = moment(ShipmentData.reference_date, SiteConfig.FRONTEND_DATE_DATA_FORMAT_MOMENT)._d;

        self.constancyDiff = ko.computed(function () {
            if (self.wellReading()) {
                return Math.abs(+self.wellReading() - +self.wellReadingRef()) / +self.wellReadingRef()
            }
            return null;
        });
        self.hasError = ko.computed(function() {
            return self.calibrationDate() && (moment(ShipmentData.reference_date, SiteConfig.FRONTEND_DATE_DATA_FORMAT_MOMENT) > moment(self.calibrationDate(), SiteConfig.FRONTEND_DATE_DATA_FORMAT_MOMENT));
        });

        self.constancyDiffDisplay = ko.computed(function () {
            let diff = self.constancyDiff();
            let sign;

            if (diff !== null) {

                sign = diff <= 0 ? "+" : "-";
                diff = (100 * diff).toFixed(1);
                return sign + diff + "% diff from ref of " + self.wellReadingRef().toPrecision(3);
            }
        });

        self.constancyFails = ko.computed(function () {

            if (self.wellReading()) {
                return self.constancyDiff() > +self.wellReadingTol();
            }
            return false;
        });

        self.addMeasurement = function (data) {
            data.calibration = self;
            self.measurements.push(new CalibrationMeasurement(data));
        };

        self.canReview = function () {
            return self.id() && !self.reviewedBy();
        };

        self.review = function () {
            const data = {
                form_type: "review_calibration",
                id: self.id(),
                password: self.password(),
                username: self.username(),
                csrfmiddlewaretoken: csrftoken
            };

            $.ajax({
                method: 'POST',
                url: AjaxUrls.shipment_cal_review.replace('0', ShipmentData.id),
                data: {'data': JSON.stringify(data)},
                success: function (data, status) {
                    self.reviewedBy(data.reviewed_by);
                    toastr['success']("Successfully reviewed calibration", "Success");
                },
                error: function (data, status) {
                    console.log('-----------------------------');
                    console.log(status);
                    console.log(data);
                    toastr['error']("Review authentication failed: " + data.responseText, "Error");
                }
            });
        };

        self.completeMeasurements = function () {
            return _.filter(self.measurements(), function (m) {
                return m.complete()
            });
        };

        self.emptyMeasurements = function () {
            return _.filter(self.measurements(), function (m) {
                return m.empty()
            });
        };

        self.canSave = function () {
            const complete = self.completeMeasurements().length;
            const empty = self.emptyMeasurements().length;
            const hasIncomplete = (complete + empty !== self.measurements().length);
            return !self.id() && (complete > 0) && !hasIncomplete;
        };

        self.avgError = ko.computed(function () {
            let err = 0, count = 0;
            const ms = self.measurements();

            for (let i = 0; i < ms.length; i++) {
                
                if (_.isNumber(ms[i].error())) {
                    err += Math.abs(ms[i].error());
                    count += 1;
                }
            }
            return count > 0 ? err / count : null;
        });
        self.avgError2 = ko.computed(function () {
            let err = 0, count = 0;
            const ms = self.measurements();

            for (let i = 0; i < ms.length; i++) {

                if (_.isNumber(ms[i].error())) {
                    err += ms[i].error();
                    count += 1;
                }
            }
            return count > 0 ? err / count : null;
        });

        self.update = function (form) {

            $.ajax({
                method: 'POST',
                url: AjaxUrls.shipment_calibration.replace('0', ShipmentData.id),
                data: {'data': JSON.stringify(self.serialize())},
                success: function (res, status) {
                    self.id(res.id);
                    toastr['success']("Successfully saved calibration", "Success");
                },
                error: function (res, status) {
                    console.log('-----------------------------');
                    console.log(status);
                    console.log(res);
                    if (res.responseText)
                        toastr['error'](res.responseText, "Error");
                    else
                        toastr['error']("Something went wrong while trying to save.", "Error");
                }
            });
        };

        self.errorFail = ko.computed(function () {
            const avgFails = _.isNumber(self.avgError()) ? Math.abs(self.avgError()) > MAX_AVG_ERR : false;
            const individualFails = _.some(_.invokeMap(self.completeMeasurements(), "errorFail"));
            return avgFails || individualFails;
        });

        self.avgErrorDisplay = ko.computed(function () {
            const err = self.avgError();
            return _.isNumber(err) ? err.toFixed(2) : "";
        });
        self.avgErrorDisplay2 = ko.computed(function () {
            const err = self.avgError2();
            return _.isNumber(err) ? err.toFixed(2) : "";
        });

        self.serialize = function () {
            const ms = _.invokeMap(self.completeMeasurements(), "serialize");
            const data = {
                shipment: ShipmentData.id,
                form_type: "calibration",
                date: moment(self.calibrationDate(), SiteConfig.FRONTEND_DATE_DATA_FORMAT_MOMENT).format(SiteConfig.FRONTEND_DATE_DATA_FORMAT_MOMENT),
                measurements: ms,
                batch_number: self.batchNumber(),
                well_reading: self.wellReading(),
                well_reading_ref: self.wellReadingRef(),
                csrfmiddlewaretoken: csrftoken
            };
            if (self.id())
                data['id'] = self.id();
            return data;
        };
        if (!self.id()) {
            for (var i = 0; i < 5; i++) {
                self.addMeasurement({});
            }
        } else {
            _.each(data.measurements, self.addMeasurement);
            for (var i = data.measurements.length; i < 5; i++) {
                self.addMeasurement({});
            }
        }
    }

    function CalibrationMeasurement(data) {

        const self = this;

        self.calibration = data.calibration;

        self.numberSeeds = ko.observable(data.number_seeds);
        self.reading = ko.observable(data.reading);

        self.measuredKerma = ko.computed(function () {
            const iType = implantTypeForItem(ShipmentItemData[0]);

            const isotope = iType.isotope;
            const skFactor = IsotopeData[isotope].air_kerma_conversion_factor;
            const mKerma = (1. / +self.numberSeeds()) * self.reading() * skFactor * DETECTOR_CAL_FACTOR;
            return !_.isNaN(mKerma) ? mKerma : null;
        });

        self.measuredKermaDisplay = ko.computed(function () {
            const sk = self.measuredKerma();
            return _.isNumber(sk) ? sk.toPrecision(4) : "";
        });


        self.expectedKerma = ko.computed(function () {

            const iType = implantTypeForItem(ShipmentItemData[0]);
            const isotope = iType.isotope;
            const decayConst = Math.log(2) / IsotopeData[isotope].half_life;
            const activityPerSeed = ShipmentData.activity_per_seed;
            const dRef = moment(ShipmentData.reference_date, SiteConfig.FRONTEND_DATE_DATA_FORMAT_MOMENT);

            let calDate;
            if (self.calibration.calibrationDate()) {
                calDate = moment(self.calibration.calibrationDate(), SiteConfig.FRONTEND_DATE_DATA_FORMAT_MOMENT);
            } else {
                return null;
            }

            const tDelta = ms2days(calDate - dRef);
            const skFactor = IsotopeData[isotope].air_kerma_conversion_factor;
            const eKerma = activityPerSeed * Math.exp(-decayConst * tDelta) * skFactor;

            return !(_.isNaN(eKerma) || (tDelta < 0)) ? eKerma : null;
        });

        self.expectedKermaDisplay = ko.computed(function () {
            const sk = self.expectedKerma();
            return _.isNumber(sk) ? sk.toPrecision(4) : "";
        });

        self.error = ko.computed(function () {
            if (+self.expectedKerma() === 0 || +self.measuredKerma() === 0) {
                return null;
            }
            const err = 100 * (self.measuredKerma() - self.expectedKerma()) / self.expectedKerma();
            return (_.isNumber(err) && !_.isNaN(err)) ? err : null;
        });

        self.errorFail = ko.computed(function () {
            return _.isNumber(self.error()) ? Math.abs(self.error()) > MAX_INDIVIDUAL_ERR : false;
        });

        self.errorDisplay = ko.computed(function () {
            return _.isNumber(self.error()) ? self.error().toFixed(2) : "";
        });

        self.empty = ko.computed(function () {
            return _.every([
                _.isNaN(parseFloat(self.reading())),
                _.isNaN(parseInt(self.numberSeeds())),
            ]);
        });

        self.complete = ko.computed(function () {
            return _.every([
                _.isNumber(self.measuredKerma()),
                _.isNumber(self.expectedKerma()),
                !_.isNaN(parseFloat(self.reading())),
                _.isNumber(self.error()),
                !_.isNaN(parseInt(self.numberSeeds())),
            ]);
        });

        self.valid = ko.computed(function () {
            return self.empty() || self.complete();
        });

        self.serialize = function () {
            return {
                measured_kerma: self.measuredKerma(),
                expected_kerma: self.expectedKerma(),
                reading: parseFloat(self.reading()),
                number_seeds: parseInt(self.numberSeeds()),
                error: self.error(),
            };
        };

    }
    
    function CalibrationViewModel(initData) {
        const self = this;
        const initCals = !_.isEmpty(initData.calibrations) ? initData.calibrations : [{cal_id: 1}];

        self.shipment = ko.observable(new Shipment(initData.shipment));
        self.calibrations = ko.observableArray([]);
        self.active_calibration_id = ko.observable(initCals[0].cal_id);

        self.addCal = function (cal_data, focus) {

            focus = _.isUndefined(focus) ? true : focus;

            cal_data.cal_id = self.calibrations().length + 1;
            self.calibrations.push(new Calibration(cal_data));
            
            if (focus) {
                $(".cal-nav-tab:last").tab("show");
            }
        };
        _.each(initCals, function (e) {
            self.addCal(e, false)
        });

        self.setActiveCal = function(id) {
            self.active_calibration_id(id);
        };

        self.isActive = function (id, p = false) {
            if (p)
                console.log(id, self.active_calibration_id())
            return self.active_calibration_id() === id;
        }
    }

    function PatientImplant(data) {
        const self = this;

        self.id = ko.observable(data.id || null);
        self.patientID = ko.observable(data.patient_id);
        self.implanted = ko.observable(data.implanted);
        self.numberSeeds = ko.observable(data.number_seeds);
        self.numberSeedsStored = ko.observable();

        self.storageVial = ko.observable();

        if (data.stored_seeds && data.stored_seeds.length > 0) {
            self.numberSeedsStored(data.stored_seeds[0].number_seeds);
            self.storageVial(data.stored_seeds[0].storage_vial);
        }

        self.radOnc = ko.observable(data.radonc);
        self.storageValid = ko.computed(function () {
            const both_empty = !(self.numberSeedsStored() || self.storageVial()) || (self.numberSeedsStored() == 0 && !self.storageVial());
            const both_complete = self.numberSeedsStored() > 0 && self.storageVial();
            return both_empty || both_complete;
        });

        self.canSave = function () {
            return _.every([
                self.storageValid(),
                self.patientID(),
                self.implanted(),
                +self.numberSeeds() > 0,
                self.radOnc()
            ]);
        };

        self.update = function () {

            $.ajax({
                method: 'POST',
                url: AjaxUrls.shipment_implant.replace('0', ShipmentData.id),
                data: {'data': JSON.stringify(self.serialize())},
                success: function (res, status) {
                    self.id(res.implant_id);
                    toastr['success']("Successfully saved patient implant", "Success");
                },
                error: function (res, status) {
                    console.log('-----------------------------');
                    console.log(status);
                    console.log(res);
                    if (res.responseText)
                        toastr['error'](res.responseText, "Error");
                    else
                        toastr['error']("Something went wrong while trying to save.", "Error");
                }
            });
        };

        self.serialize = function () {
            return {
                implanted: moment(self.implanted(), SiteConfig.FRONTEND_DATE_DATA_FORMAT_MOMENT).format(SiteConfig.FRONTEND_DATE_DATA_FORMAT_MOMENT),
                patient_id: self.patientID(),
                number_seeds: parseInt(self.numberSeeds()),
                radonc: self.radOnc(),
                number_seeds_stored: parseInt(self.numberSeedsStored()) || 0,
                storage_vial: self.storageVial() || null,
                form_type: "patient_implant",
                csrfmiddlewaretoken: csrftoken
            };
        };
    }

    function PatientViewModel(initData) {
        const self = this;
        const initImplants = !_.isEmpty(initData.implants) ? initData.implants : [{implant_id: 1}];
        self.patients = ko.observableArray([]);
        self.storageVials = ko.observableArray(StorageVialData);
        self.radOncs = ko.observableArray(RadOncData);
        self.addImplant = function (e) {
            self.patients.unshift(new PatientImplant(e));
        };

        self.canSave = function () {
            return _.some(_.map(self.patients(), function (p) {
                return p.canSave() && !p.id();
            }));
        };

        self.saveAll = function () {
            _.each(self.patients(), function (p) {
                if (p.canSave() && !p.id()) {
                    p.update();
                }
            });
        };

        _.each(initImplants, function (e) {
            self.addImplant(e, false)
        });
    }

    let calVM, patVM;

    $(document).ready(function () {

        // const shipment = ShipmentData;
        // ShipmentData.id = ShipmentData.id;
        csrftoken = $("[name=csrfmiddlewaretoken]").val();

        calVM = new CalibrationViewModel({
            shipment: ShipmentData,
            calibrations: CalibrationData
        });

        patVM = new PatientViewModel({
            shipment: ShipmentData,
            implants: PatientImplantData
        });

        ko.applyBindings(calVM, $("#calibration-container").get(0));
        ko.applyBindings(patVM, $("#patient-container").get(0));

        // $("body").on('click', ".date", function () {
        //     $(this).datepicker({
        //         format: "dd M yyyy",
        //         autoclose: true,
        //         todayHighlight: true,
        //         todayBtn: 'linked'
        //     }).datepicker('show');
        // });

        $("#cal-nav a:first").tab('show');

        $(window).bind("beforeunload", function () {
            const unsaved = $("div.btn:visible:enabled, button:visible:enabled").filter(function (index) {
                return $(this).text().toLowerCase().indexOf("save") >= 0;
            }).length;
            if (unsaved > 0) {
                return "If you leave this page now you will lose all entered values.";
            }
        });

    });
});
