$(document).ready(function(){
    "use strict";

    $("a.empty-button").click(function(evt){
        if (confirm("Are you sure you want to empty this vial?")){
            return true;
        }
        evt.preventDefault();
    });
});

