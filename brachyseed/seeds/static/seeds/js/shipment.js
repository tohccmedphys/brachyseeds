
require(['jquery', 'lodash', 'moment', 'flatpickr', 'select2'], function($, _, moment) {
    
    function mCi2MBq(mCi) {
        return 37. * mCi;
    }

    function MBq2mCi(MBq) {
        return (1. / 37.) * MBq;
    }

    let flatpickr_options = {
        dateFormat: SiteConfig.FRONTEND_DATE_DATA_FORMAT,
        altFormat: SiteConfig.FRONTEND_DATE_DISPLAY_FORMAT,
        altInput: true,
        allowInput: true
        // wrap: true
    };

    $(document).ready(function () {
        "use strict";
        $("#id_reference_date").flatpickr(flatpickr_options);
        $("#id_received").flatpickr(_.extend(flatpickr_options, { maxDate: new Date() }));
        $('.select2').select2({
            minimumResultsForSearch: 10,
            width: '100%'
        });

        const implantInputs = $("#seed-input-wrapper").find("input, select");
        const numbers = $("[name^='form-'][name$='-number']");

        const setTotals = function () {
            let totalSeeds = 0;
            let totalActivity = 0;
            let i;
            let cur;
            let implantTypeID, implantType;
            let activity_per_seed = parseFloat($("#id_activity_per_seed_mci").val()) || 0;
            activity_per_seed = mCi2MBq(activity_per_seed);

            for (i = 0; i < numbers.length; i++) {
                cur = parseInt(numbers[i].value) || 0;
                implantTypeID = $(numbers[i]).parents('.form-group').find('select').val();
                implantType = _.find(Shipment.implantTypes, function (it) {
                    return it.id === +implantTypeID;
                });
                if (cur && implantType) {
                    totalSeeds += cur * implantType.seeds_per_implant;
                }
            }

            totalActivity = activity_per_seed * totalSeeds;

            $("#total-seeds").html(totalSeeds);
            $("#total-activity-mbq").html(totalActivity.toPrecision(4));
            $("#total-activity-mci").html(MBq2mCi(totalActivity).toPrecision(4));
        };

        const checkSurface = function () {
            const el = $(this);
            const surface = parseFloat($(this).val()) || 999999999999;
            el.parents(".form-group").removeClass("has-warning");
            el.parent().find(".tol-exceeded").remove();

            if (el.val() === "") {
                return;
            }
            if (surface > Shipment.surfaceLimit) {
                el.parents(".form-group").addClass("has-warning");
                el.after(
                    '<span class="tol-exceeded help-block">Surface reading exceeds tolerance (' + Shipment.surfaceLimit + 'uR/h). Contact RSO and GE Health Canada</span>'
                );
            }

        };

        const checkDoseRate = function () {
            const el = $(this);
            const doseRate = parseFloat(el.val()) || 999999999999;
            el.parents(".form-group").removeClass("has-warning");
            el.parent().find(".tol-exceeded").remove();
            if (el.val() === "") {
                return;
            }
            if (doseRate > Shipment.doseRate1mLimit) {
                el.parents(".form-group").addClass("has-warning");
                el.after(
                    '<span class="tol-exceeded help-block">Dose rate at 1m exceeds tolerance (' + Shipment.doseRate1mLimit + 'uR/h). Contact RSO and GE Health Canada</span>'
                );
            }

        };

        setTotals();

        implantInputs.add("#id_activity_per_seed").change(setTotals);

        $("#id_surface").change(checkSurface).trigger("change");
        $("#id_dose_rate_1m").change(checkDoseRate).trigger("change");

        $("#id_activity_per_seed_mci").change(setTotals);

    });
});
