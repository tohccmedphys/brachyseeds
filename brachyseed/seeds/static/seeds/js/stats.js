
require(['jquery', 'lodash', 'moment', 'd3', 'toastr', 'datatables', 'bootstrap-colorpicker', 'daterangepicker'], function($, _, moment, d3, toastr, Datatable) {

    const colours = {
        implants: 'rgba(0, 166, 90, 0.6)',
        well_readings: 'rgba(60, 141, 188, 0.6)',
        well_reference: 'rgba(0, 192, 239, 0.6)',
        avg_error: 'rgba(221, 75, 57, 0.6)',
        expected_measurement: 'rgba(96, 92, 168, 0.6)',
        avg_measurements: 'rgba(0, 31, 63, 0.6)',
        calibration_measurements: 'rgba(0, 31, 63, 0.6)',
        received: 'rgba(243, 156, 18, 0.6)'
    };

    function get_colour(what) {
        return colours[what];
    }

    const $imp_colour = $('#imp-colour'),
        $rec_colour = $('#rec-colour'),
        $reading_colour = $('#reading-colour'),
        $reference_colour = $('#reference-colour'),
        $error_colour = $('#error-colour'),
        $measurement_colour = $('#measurement-colour'),
        $expected_colour = $('#expected-colour');

    $(window).click(function() {
        $('.colorpicker-visible').fadeOut(
            200,
            function() {
                $($imp_colour, $rec_colour, $reading_colour, $reference_colour, $error_colour).colorpicker('hide');
            }
        );
    });
    $.fn.colorpicker.constructor.prototype.repositionToMouse = function(e) {
        let _this = this;
        this.picker.fadeIn(200)
        _this.show();
        if (_this.options.inline !== false || _this.options.container) {
            return false;
        }

        $(_this.picker).css({
            position: 'absolute',
            top: e.originalEvent.clientY + 7,
            left: e.originalEvent.clientX - 134
        });

    };

    const oldest_cal_moment = moment().year(oldest_cal.year).month(oldest_cal.month - 1).startOf('month'),
        oldest_imp_moment = moment().year(oldest_imp.year).month(oldest_imp.month - 1).startOf('month');

    const ranges = {
        "Last 6 Months": [
            moment().subtract(5, 'months').startOf('month'),
            moment().endOf('month')
        ],
        "Last 12 Months": [
            moment().subtract(11, 'months').startOf('month'),
            moment().endOf('month')
        ],
        "Year To Date": [
            moment().startOf('year'),
            moment().endOf('month')
        ],
        "Previous Year": [
            moment().subtract(1, 'years').startOf('year'),
            moment().subtract(1, 'years').endOf('year')
        ],
        "Last 2 Years": [
            moment().subtract(23, 'months').startOf('month'),
            moment().endOf('month')
        ],
        "All Time": [
            oldest_cal_moment < oldest_imp_moment ? oldest_cal_moment : oldest_imp_moment,
            moment().endOf('month')
        ]
    };

    let stat_date_from, stat_date_to;
    const $stat_range = $('#stat-range');

    $stat_range.daterangepicker(
        {
            autoClose: true,
            autoApply: true,
            showDropdowns: true,
            keyboardNavigation: false,
            linkedCalendars: false,
            locale: {"format": "MMM YYYY"},
            ranges: ranges,
            startDate: moment().subtract(11, 'months').startOf('month'),
            endDate: moment().endOf('month')
        },
        getStatistics
    );

    const show_error_ticks = true;

    let dates;
    const range_data = {};
    for (let k in ranges) { range_data[ranges[k]] = { data: false } };

    function getStatistics() {

        stat_date_from = $stat_range.data('daterangepicker').startDate;
        stat_date_to = $stat_range.data('daterangepicker').endDate;

        dates = {
            from_year: stat_date_from._d.getFullYear(),
            from_month: stat_date_from._d.getMonth() + 1,
            to_year: stat_date_to._d.getFullYear(),
            to_month: stat_date_to._d.getMonth() + 1
        };

        // If data for this date range has yet to be gathered, get it
        if (!range_data[[stat_date_from, stat_date_to]].data) {

            $.ajax({
                url: urls.go_daterange,
                data: dates
            }).done(function (res_data, res_message, res) {

                range_data[[stat_date_from, stat_date_to]].data = {
                    implant_data: res_data.implant_data,
                    cal_reading_data: res_data.cal_reading_data,
                    cal_err_data: res_data.cal_err_data,
                    received_data: res_data.received_data
                };
                update_all(range_data[[stat_date_from, stat_date_to]].data);
            });
        // else use the already gathered data
        } else {
            update_all(range_data[[stat_date_from, stat_date_to]].data);
        }
    }
    getStatistics();

    function update_all(all_data) {
        update_implant_table(all_data.implant_data);
        update_implant_chart(all_data.implant_data);
        update_cal_reading_chart(all_data.cal_reading_data);
        update_cal_error_chart(all_data.cal_err_data);
        update_received_table(all_data.received_data);
        update_received_chart(all_data.received_data);
        update_datatable(all_data);
    }

    function update_implant_table(imp_data) {
        
        $('#td-pat-imp > div').fadeOut('fast', function() {
            $(this).text(imp_data.patients_implanted);
            $(this).fadeIn('fast');
        });
        $('#td-see-imp > div').fadeOut('fast', function() {
            $(this).text(imp_data.seeds_implanted);
            $(this).fadeIn('fast');
        });
        $('#td-see-per-pat > div').fadeOut('fast', function() {
            $(this).text(imp_data.seeds_per_patient);
            $(this).fadeIn('fast');
        });
    }

    function update_received_table(rec_data) {
        $('#td-shi-rec > div').fadeOut('fast', function() {
            $(this).text(rec_data.shipments);
            $(this).fadeIn('fast');
        });
        $('#td-see-rec > div').fadeOut('fast', function() {
            $(this).text(rec_data.seeds_received);
            $(this).fadeIn('fast');
        });
        $('#td-act-see-rec > div').fadeOut('fast', function() {
            $(this).text(rec_data.total_activity);
            $(this).fadeIn('fast');
        });
    }

    $imp_colour.colorpicker({color: colours.implants, format: 'rgb'}).on('colorpickerChange', function(e) {
        colours.implants = e.color.toRgbString();
        $('.imp-bar').css('fill', colours.implants);
    });
    $rec_colour.colorpicker({color: colours.received}).on('colorpickerChange', function(e) {
        colours.received = e.color.toRgbString();
        $('.rec-bar').css('fill', colours.received);
    });
    $reading_colour.colorpicker({color: colours.well_readings}).on('colorpickerChange', function(e) {
        colours.well_readings = e.color.toRgbString();
        $('.well_readings-line, .well_readings-circle').css('stroke', colours.well_readings);
        $('.well_readings-legend').css('fill', colours.well_readings);
    });
    $reference_colour.colorpicker({color: colours.well_reference}).on('colorpickerChange', function(e) {
        colours.well_reference = e.color.toRgbString();
        $('.well_reference-line, .well_reference-circle').css('stroke', colours.well_reference);
        $('.well_reference-legend').css('fill', colours.well_reference);
    });
    $error_colour.colorpicker({color: colours.avg_error}).on('colorpickerChange', function(e) {
        colours.avg_error = e.color.toRgbString();
        $('.line-avg_error, g.y.axis.yaxis2 g.tick line').css('stroke', colours.avg_error);
        $('.legend-box-avg_error').css('fill', colours.avg_error);
    });
    $measurement_colour.colorpicker({color: colours.avg_measurements}).on('colorpickerChange', function(e) {
        colours.avg_measurements = e.color.toRgbString();
        colours.calibration_measurements = colours.avg_measurements;
        $('.line-avg_measurements, .circle-measurement').css('stroke', colours.avg_measurements);
        $('.legend-box-calibration_measurements').css('fill', colours.avg_measurements);
    });
    $expected_colour.colorpicker({color: colours.expected_measurement}).on('colorpickerChange', function(e) {
        colours.expected_measurement = e.color.toRgbString();
        $('.line-expected_measurement').css('stroke', colours.expected_measurement);
        $('.legend-box-expected_measurement').css('fill', colours.expected_measurement);
    });
    $('.colorpicker').click(function (e) {
        e.stopPropagation();
    });

    function update_implant_chart(implant_data) {

        const data = $.extend({}, implant_data.implant_ticks);

        // Fill in empty months for implant data
        var m = dates.from_month;
        for (var y = dates.from_year; y <= dates.to_year; y++) {
            if (!(y in data)) { data[y] = {}; }
            while (m < 13 && (y == dates.to_year ? m <= dates.to_month : true)) {
                if (!(m in data[y])) { data[y][m] = 0; }
                m++;
            }
            m = 1;
        }

        d3.select('#implant-chart-title')
            .transition()
            .duration(200)
            .style('opacity', '0')
            .on('end', function() {
                d3.select('#implant-chart-title')
                    .text('Patients Implanted per Month (' + stat_date_from.format('MMM YYYY') + ' to ' + stat_date_to.format('MMM YYYY') + ')')
                    .transition()
                    .duration(200)
                    .style('opacity', '1');
            });

        d3.select('#implant-chart svg').remove();
        const months = [];
        for (var y in data) {
            for (var m in data[y]) {
                months.push({x: moment().year(y).month(m - 1).date(1).hour(0).minute(0).second(0).millisecond(0), y: data[y][m]});
            }
        }
        const chart_width = $('#wellreading-chart').width();
        const margin = {top: 20, right: 20, bottom: 55, left: 40},
            width = chart_width - margin.left - margin.right,
            height = 200 - margin.top - margin.bottom;

        const xScale = d3.scaleBand().rangeRound([0, width]).padding(.05);
        const yScale = d3.scaleLinear().range([height, 0]);

        const xAxis = d3.axisBottom(xScale)
                .tickFormat(function (d) {
                    return moment(d).month() == 0 ? moment(d).format('YYYY MMM') : moment(d).format('MMM')
                }),
            yAxis = d3.axisLeft(yScale)
                .tickFormat(function (d) {
                    if (d % 1 === 0) return d; else return '';
                });

        const svg = d3.select("#implant-chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        xScale.domain(d3.range(months.length).map(function(i){
            return moment(stat_date_from).add(i, 'month').valueOf()
        }));

        yScale.domain([0, d3.max(months, function(d) { return d.y; })]);

        svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
            .selectAll("text")
                .style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", "-.55em")
                .attr("transform", "rotate(-90)" );

        svg.append("g")
                .attr("class", "y axis")
                .call(yAxis);

        const colour = get_colour('implants', 0);
        const implant_bars = svg.selectAll("bar")
            .data(months);

        implant_bars.enter()
            .append("rect")
            .attr('class', 'imp-bar')
            .style("fill", colour)
            .attr("x", function(d) { return xScale(d.x); })
            .attr("width", xScale.bandwidth())
            .attr("y", yScale(0))
            .attr("height", 0)
            .transition()
            .duration(400)
            .attr("y", function(d) { return yScale(d.y); })
            .attr("height", function(d) { return height - yScale(d.y); });

        $('.imp-bar').click(function(e) {
            e.stopPropagation();
            $imp_colour.colorpicker('repositionToMouse', e);
        });
    }

    function update_cal_reading_chart(cal_r_data) {

        const data = cal_r_data;

        d3.select('#wellreading-chart-title')
            .transition()
            .duration(200)
            .style('opacity', '0')
            .on('end', function() {
                d3.select('#wellreading-chart-title')
                    .text('Well Calibration Reading (' + stat_date_from.format('MMM YYYY') + ' to ' + stat_date_to.format('MMM YYYY') + ')')
                    .transition()
                    .duration(200)
                    .style('opacity', '1');
            });

        d3.select('#wellreading-chart svg').remove();

        const chart_width = $('#wellreading-chart').width();

        const margin = {top: 20, right: 20, bottom: 55, left: 40},
            width = chart_width - margin.left - margin.right,
            height = 200 - margin.top - margin.bottom;

        const circle_radius = 2.5,
            line_width = 2;

        const legend_height = 30,
            legend_width = 100;

        const xScale = d3.scaleTime().range([0, width]);
        const yScale = d3.scaleLinear().range([height, 0]);

        const xAxis = d3.axisBottom(xScale)
                .tickFormat(function (d) {
                    return moment(d).month() == 0 ? moment(d).format('YYYY MMM') : moment(d).format('MMM')
                }),
            yAxis = d3.axisLeft(yScale).tickFormat(function (d) {
                return d3.format('.3f')(d);
            }).ticks(8);

        const svg = d3.select("#wellreading-chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        xScale.domain([stat_date_from.valueOf(), stat_date_to.valueOf()]);

        let minY = d3.min(data, function (d) {
                return d3.min(d.values, function (d2) {
                    return d2.y
                });
            }),
            maxY = d3.max(data, function (d) {
                return d3.max(d.values, function (d2) {
                    return d2.y
                });
            });

        minY = minY - (maxY - minY) / 10;
        maxY = maxY + (maxY - minY) / 10;
        yScale.domain([minY, maxY]);

        yAxis.tickSizeInner(-width).tickSizeOuter(0).tickValues(d3.range(minY, maxY, (maxY - minY) / 10));

        const line = d3.line()
            .curve(d3.curveLinear)
            .x(function (d) {
                return xScale(d.x);
            })
            .y(function (d) {
                return yScale(d.y);
            });

        svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
            .selectAll("text")
                .style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", "-.55em")
                .attr("transform", "rotate(-90)" );

        svg.append("g")
                .attr("class", "y axis")
                .call(yAxis)
            .append("text");

        const legend = svg.append('g')
            .attr('class', 'legend')
            .attr('x', width - legend_width)
            .attr('y', legend_height)
            .attr('height', legend_height)
            .attr('width', legend_width);

        const legend_entry = legend.selectAll('rect')
            .data(data).enter();

        legend_entry.append("rect")
            .attr("x", width - legend_width)
            .attr("y", function(d, i){ return i * 20 - 10;})
            .attr("width", 10)
            .attr("height", 10)
            .attr('class', function(d) {return d.name + '-legend'})
            .style('stroke', '#000000')
            .style('stroke-width', '0.3px')
            .style("fill", function(d) {
                return get_colour(d.name);
            });

        legend_entry.append('text')
            .attr("x", 20 + width - legend_width)
            .attr("y", function(d, i){ return i *  20;})
            .text(function(d) { return d.legend });

        // Clip rect for animating lines
        const curtain = svg.append('clipPath')
            .attr('id', 'cal-curtain')
            .append("rect")
            .attr("id", "cal-rectmask")
            .attr('height', height);

        const cal_series = svg.selectAll(".cal_series")
            .data(data)
            .enter()
            .append('g')
            .attr('class', 'cal_series');

        const cal_results = cal_series.append('g')
            .attr('class', 'cal_results');

        cal_results.append("path")
            .attr("clip-path", "url(#cal-curtain)")
            .attr('class', function(d) {return 'cal_line ' + d.name + '-line'})
            .style('fill', 'none')
            .style("stroke", function(d) { return get_colour(d.name) })
            .attr('stroke-width', line_width)
            .attr("d", function(d) { return line(d.values); });

        cal_results.selectAll('circle').data(function(d) { return d.values; })
            .enter().append('circle')
                .attr('class', function(d, i, s) { return s[i].parentNode.__data__.name + '-circle'})
                .attr("clip-path", "url(#cal-curtain)")
                .attr("stroke-width", 1)
                .attr("stroke", function(d, i, s) { return get_colour(s[i].parentNode.__data__.name); })
                .attr("cx", function (d) { return xScale(d.x); })
                .attr("cy", function (d) { return yScale(d.y); })
                .attr("r", circle_radius)
                .attr("fill", "white").attr("fill-opacity", .5);

        d3.select('#cal-rectmask')
            .attr('width', 0)
            .transition()
            .duration(400)
            .attr('width', width);

        $('.well_readings-legend').click(function(e) {
            e.stopPropagation();
            $reading_colour.colorpicker('repositionToMouse', e);
        });
        $('.well_reference-legend').click(function(e) {
            e.stopPropagation();
            $reference_colour.colorpicker('repositionToMouse', e);
        });

    }

    function update_cal_error_chart(cal_err_data) {

        const data = cal_err_data;

        d3.select('#calibration-chart-title')
            .transition()
            .duration(200)
            .style('opacity', '0')
            .on('end', function() {
                d3.select('#calibration-chart-title')
                    .text('Calibration Data (' + stat_date_from.format('MMM YYYY') + ' to ' + stat_date_to.format('MMM YYYY') + ')')
                    .transition()
                    .duration(200)
                    .style('opacity', '1');
            });

        d3.select('#calibration-chart svg').remove();

        const chart_width = $('#calibration-chart').width();

        const legend_height = 30,
            legend_width = 200;

        const margin = {top: 20 + legend_height, right: 40, bottom: 100, left: 40},
            width = chart_width - margin.left - margin.right,
            height = 250 - margin.top + legend_height - margin.bottom;

        const circle_radius = 1.5,
            line_width = 2;

        const xScale = d3.scaleTime().range([0, width]);
        const yScale = d3.scaleLinear().range([height, 0]);
        const yScale2 = d3.scaleLinear().range([height, 0]);

        const y_font_size = 12;

        const xAxis = d3.axisBottom(xScale)
                .tickFormat(function (d) {
                    return moment(d).month() === 0 ? moment(d).format('YYYY MMM') : moment(d).format('MMM')
                }),
            yAxis = d3.axisLeft(yScale).tickFormat(function (d) {
                return d3.format('.3f')(d);
            }).ticks(8),
            yAxis2 = d3.axisRight(yScale2).tickFormat(function (d) {
                return d % 1 === 0 ? d : '';
            });

        const svg = d3.select("#calibration-chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        xScale.domain([stat_date_from.valueOf(), stat_date_to.valueOf()]);

        let minY = d3.min([data.avg_measurements, data.calibration_measurements, data.expected_measurement], function (d) {
                return d3.min(d.values, function (d2) {
                    return d2.y
                })
            }),
            maxY = d3.max([data.avg_measurements, data.calibration_measurements, data.expected_measurement], function (d) {
                return d3.max(d.values, function (d2) {
                    return d2.y
                })
            }),
            minY2 = d3.min([data.avg_error], function (d) {
                return d3.min(d.values, function (d2) {
                    return d2.y
                })
            }),
            maxY2 = d3.max([data.avg_error], function (d) {
                return d3.max(d.values, function (d2) {
                    return d2.y
                })
            });

        minY = minY - (maxY - minY) / 10;
        maxY = maxY + (maxY - minY) / 10;
        minY2 = minY2 - (maxY2 - minY2) / 10;
        maxY2 = maxY2 + (maxY2 - minY2) / 10;

        yScale.domain([minY, maxY]);
        yScale2.domain([minY2, maxY2]);
        yAxis.tickSizeInner(-width).tickSizeOuter(0);
        yAxis2.tickSizeInner(-width).tickSizeOuter(0);//.tickValues(d3.range(Math.round(minY), Math.ceil(maxY), Math.round((maxY - minY) / 10)));

        const line = d3.line()
            .curve(d3.curveLinear)
            .x(function (d) {
                return xScale(d.x);
            })
            .y(function (d) {
                return yScale(d.y);
            })
            .defined(function (d) {
                return d.y !== null;
            });

        const line2 = d3.line()
            .curve(d3.curveLinear)
            .x(function (d) {
                return xScale(d.x);
            })
            .y(function (d) {
                return yScale2(d.y);
            })
            .defined(function (d) {
                return d.y !== null;
            });

        svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
            .selectAll("text")
                .style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", "-.55em")
                .attr("transform", "rotate(-90)" );

        svg.append("g")
                .attr("class", "y axis")
                .call(yAxis);

        svg.append("g")
                .attr("class", "y axis yaxis2")
                .attr("transform", "translate(" + width + ",0)")
                .call(yAxis2);

        svg.append("text")
            .attr("text-anchor", "middle")
            .attr("transform", "translate(" + (width + margin.right) + "," + (height / 2) + ")rotate(-90)")
            .text("% Error");

        const mouse_tracker = svg.append("rect")
            .attr("width", width)
            .attr("height", height)
            .attr("id", "mouse-tracker")
            .style("fill", 'transparent')
            .on('click', function () {
                show_error_ticks = !show_error_ticks;
                update_cal_error_chart(cal_err_data);
            });

        const legend = svg.append('g')
            .attr('class', 'legend')
            // .attr('x', width + margin.right)
            // .attr('y', legend_height + 20)
            .attr("transform", "translate(0," + (-margin.top + 10) + ")")
            .attr('height', legend_height)
            .attr('width', legend_width);

        const legend_entry = legend.selectAll('rect')
            .data([data.avg_error, data.calibration_measurements, data.expected_measurement]).enter();

        legend_entry.append("rect")
            .attr("x", width - legend_width)
            .attr("y", function(d, i){ return i * 20 - 10;})
            .attr("width", 10)
            .attr("height", 10)
            .attr('class', function(d) { return 'legend-box-' + d.name; })
            .style('stroke', '#000000')
            .style('stroke-width', '0.3px')
            .style("fill", function(d) { return get_colour(d.name) });

        legend_entry.append('text')
            .attr("x", 20 + width - legend_width)
            .attr("y", function(d, i){ return i *  20;})
            .text(function(d) { return d.legend; });

        // Clip path for animation
        const curtain = svg.append('clipPath')
            .attr('id', 'err-curtain')
            .append("rect")
            .attr("id", "err-rectmask")
            .attr('height', height);

        const line_series_1 = svg.selectAll(".line_series_1")
            .data([data.expected_measurement, data.avg_measurements])
            .enter()
            .append('g')
            .attr('class', 'line_series_1');

        const line_series_1_results = line_series_1.append('g')
            .attr('class', 'line_series_1_results');

        const line_series_2 = svg.selectAll(".line_series_2")
            .data([data.avg_error])
            .enter()
            .append('g')
            .attr('class', 'line_series_2');

        const line_series_2_results = line_series_2.append('g')
            .attr('class', 'line_series_2_results');

        const dot_series_1 = svg.selectAll(".dot_series_1")
            .data([data.calibration_measurements])
            .enter()
            .append('g')
            .attr('class', 'dot_series_1');

        const dot_series_1_results = dot_series_1.append('g')
            .attr('class', 'dot_series_1_results');

        line_series_1_results.append("path")
            .attr("clip-path", "url(#err-curtain)")
            .attr("class", function(d) { return 'line-' + d.name; })
            .attr("d", function(d) { return line(d.values); })
            .attr('stroke-width', line_width)
            .style("stroke", function(d) { return get_colour(d.name) })
            .style('fill', 'none');

        line_series_2_results.append("path")
            .attr("clip-path", "url(#err-curtain)")
            .attr("class", function(d) { return 'line-' + d.name; })
            .attr("d", function(d) { return line2(d.values); })
            .attr('stroke-width', line_width)
            .style("stroke", function(d) { return get_colour(d.name) })
            .style('fill', 'none');

        dot_series_1_results.selectAll('circle').data(function(d) { return d.values; })
            .enter().append('circle')
                .attr('class', 'circle-measurement')
                .attr("clip-path", "url(#err-curtain)")
                .attr("stroke-width", 1)
                .attr("stroke", function(d) { return get_colour('calibration_measurements'); })
                .attr("cx", function (d) { return xScale(d.x); })
                .attr("cy", function (d) { return yScale(d.y); })
                .attr("r", circle_radius)
                .attr("fill", "white")
                .attr("fill-opacity", 0);

        d3.select('#err-rectmask')
            .attr('width', 0)
            .transition()
            .duration(400)
            .attr('width', width);

        $('.legend-box-expected_measurement').click(function(e) {
            e.stopPropagation();
            $expected_colour.colorpicker('repositionToMouse', e);
        });

        $('.legend-box-calibration_measurements').click(function(e) {
            e.stopPropagation();
            $measurement_colour.colorpicker('repositionToMouse', e);
        });

        $('.legend-box-avg_error').click(function(e) {
            e.stopPropagation();
            $error_colour.colorpicker('repositionToMouse', e);
        });

        d3.selectAll("g.y.axis.yaxis2 g.tick line")
            .attr("x1", -width)
            .transition()
            .duration(400)
            .style('stroke', function(d) { return get_colour('avg_error')})
            .style('stroke-width', 1)
            .style('opacity', 1)
            .style('stroke-dasharray', function(d){
                if (d === 0)
                    return '2,2';
                else if (d === -5 || d === 5) return '8,8';
                else return null;
            })
            .attr("x1", function(d){
                if (d === 0 || d === -5 || d === 5)
                    return show_error_ticks ? 0 : -width;
                else return -width;
            });

    }

    function update_received_chart(rec_data) {

        const data = $.extend({}, rec_data.shipment_ticks);

        // Fill in empty months for implant data
        var m = dates.from_month;
        for (var y = dates.from_year; y <= dates.to_year; y++) {
            if (!(y in data)) { data[y] = {}; }
            while (m < 13 && (y == dates.to_year ? m <= dates.to_month : true)) {
                if (!(m in data[y])) { data[y][m] = 0; }
                m++;
            }
            m = 1;
        }

        d3.select('#received-chart-title')
            .transition()
            .duration(200)
            .style('opacity', '0')
            .on('end', function() {
                d3.select('#received-chart-title')
                    .text('Shipments Received per Month (' + stat_date_from.format('MMM YYYY') + ' to ' + stat_date_to.format('MMM YYYY') + ')')
                    .transition()
                    .duration(200)
                    .style('opacity', '1');
            });

        d3.select('#received-chart svg').remove();
        const months = [];
        for (var y in data) {
            for (var m in data[y]) {
                months.push({x: moment().year(y).month(m - 1).date(1).hour(0).minute(0).second(0).millisecond(0), y: data[y][m]});
            }
        }
        const chart_width = $('#received-chart').width();
        const margin = {top: 20, right: 20, bottom: 55, left: 40},
            width = chart_width - margin.left - margin.right,
            height = 200 - margin.top - margin.bottom;

        const xScale = d3.scaleBand().rangeRound([0, width]).padding(.05);
        const yScale = d3.scaleLinear().range([height, 0]);

        const xAxis = d3.axisBottom(xScale)
                .tickFormat(function (d) {
                    return moment(d).month() == 0 ? moment(d).format('YYYY MMM') : moment(d).format('MMM')
                }),
            yAxis = d3.axisLeft(yScale)
                .tickFormat(function (d) {
                    if (d % 1 === 0) return d; else return '';
                });

        const svg = d3.select("#received-chart").append("svg")
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
            .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

        xScale.domain(d3.range(months.length).map(function(i){
            return moment(stat_date_from).add(i, 'month').valueOf()
        }));

        yScale.domain([0, d3.max(months, function(d) { return d.y; })]);

        svg.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + height + ")")
                .call(xAxis)
            .selectAll("text")
                .style("text-anchor", "end")
                .attr("dx", "-.8em")
                .attr("dy", "-.55em")
                .attr("transform", "rotate(-90)" );

        svg.append("g")
                .attr("class", "y axis")
                .call(yAxis);

        const colour = get_colour('received', 0);
        const implant_bars = svg.selectAll("bar")
            .data(months);

        implant_bars.enter()
            .append("rect")
            .style("fill", colour)
            .attr('class', 'rec-bar')
            .attr("x", function(d) { return xScale(d.x); })
            .attr("width", xScale.bandwidth())
            .attr("y", yScale(0))
            .attr("height", 0)
            .transition()
            .duration(400)
            .attr("y", function(d) { return yScale(d.y); })
            .attr("height", function(d) { return height - yScale(d.y); });

        $('.rec-bar').click(function(e) {
            e.stopPropagation();
            $rec_colour.colorpicker('repositionToMouse', e);
        });
    }

    const $dt = $('#data-table');
    $dt.hide();
    $dt.dataTable({
        // aaData: data,
        bPaginate: false,
        bFilter: false,
        columns: [
            { title: "Date" },
            { title: "Reading" },
            { title: "Reference" },
            { title: "Average Error" }
        ]
    });

    let csv_data;

    function update_datatable(all_data) {

        const datadict = {};
        for (var i in all_data.cal_err_data.avg_error.values) {
            datadict[all_data.cal_err_data.avg_error.values[i].x] = {
                'error': all_data.cal_err_data.avg_error.values[i].y
            }
        }
        for (let j = 0; j < all_data.cal_reading_data.length; j++) {
            for (var i = 0; i < all_data.cal_reading_data[j].values.length; i++) {
                datadict[all_data.cal_reading_data[j].values[i].x][all_data.cal_reading_data[j].name] = all_data.cal_reading_data[j].values[i].y;
            }
        }

        const data = [];
        for (let d in datadict) {
            data.push([moment(parseInt(d)).format('MMM D YYYY'), datadict[d].well_readings.toFixed(3), datadict[d].well_reference.toFixed(3), datadict[d].error.toFixed(3)]);
        }

        $dt.fadeOut(200, function() {
            $dt.fnClearTable();
            $dt.fnAddData(data);
            $dt.fadeIn(200);
        });
        csv_data = data;
    }

    $('#export-csv').click(function() {
        const lineArray = [];
        csv_data.forEach(function (infoArray, index) {
            const line = infoArray.join(",");
            lineArray.push(index == 0 ? "data:text/csv;charset=utf-8,Date,Reading,Reference,Average Error\n" + line : line);
        });
        const csvContent = lineArray.join("\n");

        const encodedUri = encodeURI(csvContent);
        const link = document.createElement("a");
        link.setAttribute("href", encodedUri);
        link.setAttribute("download", "calibration_data_" + moment(stat_date_from).format('DD/MM/YYYY') + "_to_" + moment(stat_date_to).format('DD/MM/YYYY') + ".csv");
        document.body.appendChild(link); // Required for FF

        link.click();

    });

    let toastr_question_options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "0",
        "extendedTimeOut": "0",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        // "tapToDismiss": true
    };

    let toastr_notify_options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "3000",
        "extendedTimeOut": "5000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        // "tapToDismiss": true
    };

    $('.empty-button').click(function() {
        let id = $(this).attr('data-id');
        let url = AjaxUrls.empty_vial.replace('0', id);
        let vial_row = $('#vial-row-' + id);

        toastr.options = toastr_question_options;

        toastr['info'](
            '<br><div class="row"><div class="col-lg-12"><div id="empty-btn" class="btn btn-flat btn-primary clear float-right">Yes</div>' +
            '<div class="btn btn-flat btn-default clear">Cancel</div></div></div>',
            'Empty vial ' + id + '?'
        )
        let empty_btn = $('#empty-btn');
        empty_btn.unbind('click');
        empty_btn.click(function() {
            $.ajax({
                url: url,
                method: 'POST',
                success: function(res) {
                    toastr.options = toastr_notify_options;
                    toastr['success'](res.message);
                    claerVials(res.id, res.last_emptied);
                },
                error: function (res) {
                    toastr.options = toastr_notify_options;
                    toastr['error'](res);
                }
            })
        })
    })

    function claerVials(id, last_emptied) {
        // $('#vial-row-' + id + ' td.vial-id').html('0');
        $('#vial-row-' + id + ' td.vial-seeds').html('0');
        $('#vial-row-' + id + ' td.vial-act').html('0.000');
        $('#vial-row-' + id + ' td.date-emptied').html(last_emptied);
    }


    $(window).resize(function() {
        getStatistics();
    });

    // TODO d3:
    // $.plot("#implant-chart", [implantCountData], implantOptions);
    // $.plot("#wellreading-chart", [{'label': "Reading", data: calStats.well_readings}, {
    //     label: "Reference",
    //     data: calStats.well_reading_refs
    // }], wellOptions);
    // $.plot("#calibration-chart", [calStats.errors], wellOptions);

});