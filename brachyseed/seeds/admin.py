from django.contrib import admin
from seeds import models


class PatientImplantAdmin(admin.ModelAdmin):
    list_display = ("pk", "shipment", "patient_id", "implanted","radonc")


class ShipmentItemAdmin(admin.ModelAdmin):
    list_display = ("pk", "shipment", "implant_type", "number")


class ShipmentAdmin(admin.ModelAdmin):
    list_display = ("po_number", "received", "received_by", "reference_date", "activity_per_seed", )


def patient(obj):
    return obj.patient_implant.patient_id


class StorageVialSeedsAdmin(admin.ModelAdmin):
    list_display = ("pk", "shipment", patient, "number_seeds", "date_stored", "storage_vial", "disposed")


class CalibrationAdmin(admin.ModelAdmin):
    list_display = ("pk", "shipment", "batch_number", "well_reading", "well_reading_ref", "measured", "measured_by", "reviewed", "reviewed_by")

    def get_queryset(self, request):
        qs = super(CalibrationAdmin, self).get_queryset(request)
        return qs.select_related("measured_by", "reviewed_by", "shipment")


def calibration(obj):
    return "%d - %s" %(obj.calibration.pk, obj.calibration.shipment)


class CalibrationMeasurementAdmin(admin.ModelAdmin):
    list_display = ("pk", calibration, "number_seeds", "measured_kerma", "expected_kerma", "error",)

    def get_queryset(self, request):

        qs = super(CalibrationMeasurementAdmin, self).get_queryset(request)
        return qs.select_related("calibration", "calibration__shipment")


admin.site.register([models.PatientImplant], PatientImplantAdmin)
admin.site.register([models.ShipmentItem], ShipmentItemAdmin)
admin.site.register([models.Shipment], ShipmentAdmin)
admin.site.register([models.StorageVialSeeds], StorageVialSeedsAdmin)
admin.site.register([models.Calibration], CalibrationAdmin)
admin.site.register([models.CalibrationMeasurement], CalibrationMeasurementAdmin)

admin.site.register([
    models.ImplantType,
    models.StorageVial,
    models.RadOnc,
], admin.ModelAdmin)
