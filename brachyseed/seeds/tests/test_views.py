from django.test import TestCase, LiveServerTestCase
from .. import models

from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.http import Http404
from django.test.client import Client

from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait

import datetime
import django.utils.timezone as timezone

import random


class BaseSeedLiveTest(LiveServerTestCase):
    WAIT = 10

    @classmethod
    def setUpClass(cls):
        cls.selenium = WebDriver()
        super(BaseSeedLiveTest, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        import time
        time.sleep(6)
        cls.selenium.quit()
        super(BaseSeedLiveTest, cls).tearDownClass()

    def setUp(self):
        super(BaseSeedLiveTest, self).setUp()

        self.username = "user"
        self.password = "pwd"
        self.user = User.objects.create_superuser(self.username, "a@b.com", self.password)

        url = '%s%s' % (self.live_server_url, "/admin/")
        self.selenium.get(url)
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys(self.user.username)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys(self.password)

        self.selenium.find_element_by_xpath('//input[@value="Log in"]').click()
        WebDriverWait(self.selenium, self.WAIT).until(lambda driver: driver.find_element_by_tag_name('body'))


class TestShipment(BaseSeedLiveTest):
    def setUp(self):
        super(TestShipment, self).setUp()

        self.seeds_per_loose = 1
        self.seeds_per_rapid = 10
        self.loose = models.ImplantType.objects.create(isotope=models.I125, name="loose seed", seeds_per_implant=self.seeds_per_loose)
        self.rapid = models.ImplantType.objects.create(isotope=models.I125, name="rapidstrand", seeds_per_implant=self.seeds_per_rapid)

    def fill_form(self):
        rec_date = ref_date = "1 Jan 2014"

        self.selenium.find_element_by_id("id_po_number").send_keys("123456")
        self.selenium.execute_script("document.getElementById('id_received').value='%s';" % rec_date)
        self.selenium.find_element_by_id("id_received_by").find_elements_by_tag_name("option")[1].click()
        self.selenium.execute_script("document.getElementById('id_reference_date').value='%s';" % rec_date)
        self.selenium.execute_script("document.getElementById('id_activity_per_seed_mci').value='0.487';")
        self.selenium.execute_script("document.getElementById('id_surface').value='1';")
        self.selenium.execute_script("document.getElementById('id_dose_rate_1m').value='1';")

    def test_new_shipment(self):
        url = self.live_server_url + reverse("shipment")
        self.selenium.get(url)

        self.fill_form()

        self.selenium.find_element_by_id("id_form-0-implant_type").find_elements_by_tag_name("option")[1].click()
        self.selenium.execute_script("document.getElementById('id_form-0-number').value='10';")

        self.selenium.find_element_by_css_selector('button[type="submit"]').click()

        WebDriverWait(self.selenium, 10).until(lambda driver: driver.find_element_by_tag_name('body'))
        self.assertIsNotNone(self.selenium.find_element_by_class_name("alert-success"))
        date = datetime.date(day=1, month=1, year=2014)
        self.assertEquals(models.Shipment.objects.get(pk=1).received, date)
        self.assertEquals(models.ShipmentItem.objects.count(), 1)

    def test_missing_formset(self):
        url = self.live_server_url + reverse("shipment")
        self.selenium.get(url)

        self.fill_form()

        self.selenium.find_element_by_css_selector('button[type="submit"]').click()

        self.assertIsNotNone(self.selenium.find_element_by_class_name("alert-danger"))
        self.assertEquals(models.Shipment.objects.count(), 0)
        self.assertEquals(models.ShipmentItem.objects.count(), 0)


class TestShipmentDetail(BaseSeedLiveTest):
    def setUp(self):

        super(TestShipmentDetail, self).setUp()

        self.received_days_ago = 3
        self.received_date = datetime.date.today() - datetime.timedelta(days=self.received_days_ago)
        self.reference_date = self.received_date
        self.calibration_date = datetime.date.today()
        self.number_loose = 7
        self.seeds_per_loose = 1
        self.number_rapid = 4
        self.seeds_per_rapid = 11
        self.initial_activity = models.mCi2MBq(0.487)

        loose = models.ImplantType.objects.create(isotope=models.I125, name="loose seed", seeds_per_implant=self.seeds_per_loose)
        rapid = models.ImplantType.objects.create(isotope=models.I125, name="rapidstrand", seeds_per_implant=self.seeds_per_rapid)

        self.shipment = models.Shipment(
            po_number="12345",
            received=self.reference_date,
            reference_date=self.reference_date,
            received_by=self.user,
            activity_per_seed=self.initial_activity,
            surface=1,
            dose_rate_1m=1
        )
        self.shipment.save()

        self.loose_item = models.ShipmentItem(
            shipment=self.shipment,
            implant_type=loose,
            number=self.number_loose
        )
        self.rapid_item = models.ShipmentItem(
            shipment=self.shipment,
            implant_type=rapid,
            number=self.number_rapid
        )
        self.shipment_items = [self.loose_item, self.rapid_item]

        models.ShipmentItem.objects.bulk_create(self.shipment_items)

    def test_calibration(self):

        url = self.live_server_url + reverse("shipment-detail", kwargs={'pk': self.shipment.pk})
        self.selenium.get(url)

        expected_avg_err = 1.2
        expected_kerma = 0.597

        cd = self.calibration_date

        self.selenium.find_element_by_css_selector("#calibration-1 .datepicker").click()
        self.selenium.find_element_by_class_name("today").click()

        self.selenium.execute_script("document.getElementById('well-reading-1').value='1';")
        self.selenium.find_element_by_id("well-reading-1").send_keys("1")

        inputs = self.selenium.find_element_by_id(
            "calibration-1"
        ).find_elements_by_css_selector("fieldset .form-group input")

        groups = [inputs[(i - 1) * 5:i * 5] for i in range(1, len(inputs) / 5 + 1)]
        readings = sorted([29.8, 29.8, 30.8, 30.3])

        for reading, group in zip(readings, groups):
            n, r, skm, ske, e = group
            n.send_keys("2")
            r.send_keys(str(reading))
            r.send_keys("\t")

        # check to make sure avg err is calculated correctly
        avg_err_el = self.selenium.find_element_by_id("calibration-1").find_element_by_class_name("avg-error")
        avg_err = float(avg_err_el.text.split(":")[-1])
        self.assertAlmostEqual(avg_err, expected_avg_err, 1)

        self.selenium.find_element_by_id("calibration-1").find_element_by_link_text("Save Calibration").click()

        WebDriverWait(self.selenium, self.WAIT).until(lambda driver: driver.find_element_by_class_name('alert-success'))
        self.shipment = models.Shipment.objects.get(pk=self.shipment.pk)
        cal = self.shipment.latest_calibration()

        self.assertEqual(len(readings), cal.calibrationmeasurement_set.count())

        for r, cm in zip(readings, cal.calibrationmeasurement_set.order_by("reading")):
            self.assertEqual(cm.number_seeds, 2)
            r = float(r)
            sk_factor = models.ISOTOPES[models.I125]["air_kerma_conversion_factor"]
            kerma = (1. / cm.number_seeds) * cm.reading * sk_factor * settings.DETECTOR_CONVERSION_FACTOR;
            self.assertAlmostEqual(float(r), cm.reading)
            self.assertAlmostEqual(kerma, cm.measured_kerma)
            self.assertAlmostEqual(expected_kerma, int(1000 * cm.expected_kerma) / 1000., 3)
            self.assertAlmostEqual(100 * (kerma / cm.expected_kerma - 1.), cm.error)

        # now check calibration is displayed on a refresh
        url = self.live_server_url + reverse("shipment-detail", kwargs={'pk': self.shipment.pk})
        self.selenium.get(url)

        el = self.selenium.find_element_by_link_text(self.calibration_date.strftime("%d %b %Y"))
        self.assertIsNotNone(el)

        uname = self.selenium.find_element_by_id("review-username-1")
        uname.send_keys(self.username)

        pwd = self.selenium.find_element_by_id("review-password-1")
        pwd.send_keys("wrongpw")
        self.selenium.find_element_by_link_text("Mark Reviewed").click()
        WebDriverWait(self.selenium, self.WAIT).until(lambda driver: driver.find_element_by_class_name('alert-danger'))

        pwd.clear()
        pwd.send_keys(self.password)
        self.selenium.find_element_by_link_text("Mark Reviewed").click()
        WebDriverWait(self.selenium, self.WAIT).until(lambda driver: driver.find_element_by_class_name('alert-success'))

    def test_implant(self):

        rad_onc = models.RadOnc.objects.create(name="rad_onc")
        url = self.live_server_url + reverse("shipment-detail", kwargs={'pk': self.shipment.pk})
        self.selenium.get(url)

        number_implanted = 2
        number_stored = 2

        container = self.selenium.find_element_by_id("patient-container")
        ip = self.calibration_date

        container.find_element_by_link_text("Add Implant").click()

        forms = container.find_elements_by_tag_name("form")

        for pn, form in enumerate(forms):
            form.find_element_by_class_name("datepicker").click()
            self.selenium.find_element_by_css_selector(".datepicker-days").find_element_by_class_name("today").click()
            form.find_element_by_id("patient-1").send_keys(str(pn) * 8)
            # form.find_element_by_class_name("num-seeds").send_keys(str(number_implanted))
            self.selenium.execute_script('patVM.patients()[%d].numberSeeds(%d)' % (pn, number_implanted))

            form.find_element_by_class_name("rad-onc").find_elements_by_tag_name("option")[1].click()
            self.selenium.execute_script('patVM.patients()[%d].numberSeedsStored(%d)' % (pn, number_stored))
            form.find_element_by_class_name("vial").find_elements_by_tag_name("option")[1].click()
            form.find_element_by_tag_name("button").click()
            WebDriverWait(self.selenium, self.WAIT).until(
                lambda driver: driver.find_element_by_class_name('alert-success'))

        implants = models.PatientImplant.objects.all()
        self.assertEqual(implants.count(), 2)

        for pin, pi in enumerate(implants):
            self.assertEqual(pi.implanted, ip)
            self.assertEqual(pi.number_seeds, number_implanted)
            self.assertEqual(pi.patient_id, str(pin) * 8)

        # check implants displayed
        self.selenium.get(url)
        els = self.selenium.find_elements_by_link_text(self.calibration_date.strftime("%d %b %Y"))
        forms = self.selenium.find_element_by_id("patient-container").find_elements_by_tag_name("form")
        self.assertEqual(len(forms), 2)


class TestEmptyStorageVial(BaseSeedLiveTest):
    def setUp(self):
        super(TestEmptyStorageVial, self).setUp()

        self.received_days_ago = 4
        self.reference_date = datetime.date.today() - datetime.timedelta(days=self.received_days_ago)
        self.number_loose = 7
        self.seeds_per_loose = 1
        self.number_rapid = 4
        self.seeds_per_rapid = 11
        self.initial_activity = models.mCi2MBq(0.487)

        loose = models.ImplantType.objects.create(isotope=models.I125, name="loose seed", seeds_per_implant=self.seeds_per_loose)
        rapid = models.ImplantType.objects.create(isotope=models.I125, name="rapidstrand", seeds_per_implant=self.seeds_per_rapid)

        self.shipment = models.Shipment(
            po_number="12345",
            received=self.reference_date,
            reference_date=self.reference_date,
            received_by=self.user,
            activity_per_seed=self.initial_activity,
            surface=1.,
            dose_rate_1m=1.,
        )
        self.shipment.save()

        self.loose_item = models.ShipmentItem(
            shipment=self.shipment,
            implant_type=loose,
            number=self.number_loose
        )
        self.rapid_item = models.ShipmentItem(
            shipment=self.shipment,
            implant_type=rapid,
            number=self.number_rapid
        )
        self.shipment_items = [self.loose_item, self.rapid_item]

        models.ShipmentItem.objects.bulk_create(self.shipment_items)

        self.vial1 = models.StorageVial.objects.get(pk=1)

        self.number_implanted = 50
        self.number_stored = 5

        self.patient_implant = models.PatientImplant(
            shipment=self.shipment,
            patient_id="123456",
            implanted=datetime.date.today(),
            number_seeds=self.number_implanted,
            radonc=models.RadOnc.objects.create(name="rad onc")
        )
        self.patient_implant.save()

        svs = models.StorageVialSeeds(
            shipment=self.shipment,
            patient_implant=self.patient_implant,
            number_seeds=self.number_stored,
            date_stored=datetime.date.today(),
            storage_vial=self.vial1,
        )
        svs.save()

    def test_empty(self):
        cur_stats = models.StorageVialSeeds.objects.current_seed_stats()
        self.assertTrue(cur_stats['total_seeds'] > 0)
        url = self.live_server_url + reverse("stats")
        self.selenium.get(url)

        row = self.selenium.find_elements_by_css_selector("tbody tr")[0]
        row.find_element_by_link_text("Empty").click()
        self.selenium.switch_to_alert().accept()
        WebDriverWait(self.selenium, self.WAIT).until(lambda driver: driver.find_element_by_class_name('alert-success'))

        row = self.selenium.find_elements_by_css_selector("tbody tr")[0]
        date = row.find_element_by_class_name("date-emptied")
        self.assertEqual(date.text, datetime.date.today().strftime("%d %b %Y").lstrip('0'))

        cur_stats = models.StorageVialSeeds.objects.current_seed_stats()
        self.assertEqual(cur_stats['total_seeds'], 0)
