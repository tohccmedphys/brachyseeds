import datetime
import math
from django.contrib.auth.models import User
from django.forms import ValidationError
from django.test import TestCase
from django.contrib import messages
from .. import models, forms


class TestShipmentForm(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="user", password="password")

    def test_activity_converted(self):
        activity_per_seed_mci = 0.487
        activity_per_seed = models.mCi2MBq(activity_per_seed_mci)
        data = {
            'po_number': "12345",
            'received': "12 Jan 2014",
            'reference_date': "12 Jan 2014",
            'received_by': self.user.pk,
            'activity_per_seed_mci': activity_per_seed_mci,
            'surface': 1.,
            'background': 1.,
            'dose_rate_1m': 1.,
        }
        f = forms.ShipmentForm(data=data)
        shipment = f.save()
        self.assertAlmostEqual(shipment.activity_per_seed, activity_per_seed)

    # def test_ref_after_received(self):
    #     activity_per_seed_mci = 0.487
    #     data = {
    #         'po_number': "12345",
    #         'received': "12 Jan 2014",
    #         'reference_date': "14 Jan 2014",
    #         'received_by': self.user.pk,
    #         'activity_per_seed_mci': activity_per_seed_mci,
    #         'surface': 1.,
    #         'dose_rate_1m': 1.,
    #     }
    #     f = forms.ShipmentForm(data=data)
    #     with self.assertRaises(ValidationError):
    #         f.is_valid()
    #         f.clean()

    def test_received_in_future(self):
        activity_per_seed_mci = 0.487
        data = {
            'po_number': "12345",
            'received': "12 Jan 3014",
            'reference_date': "14 Jan 2014",
            'received_by': self.user.pk,
            'activity_per_seed_mci': activity_per_seed_mci,
            'surface': 1.,
            'dose_rate_1m': 1.,
        }
        f = forms.ShipmentForm(data=data)
        with self.assertRaises(ValidationError):
            f.is_valid()
            f.clean()


class TestShipmentItemForm(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="user", password="password")
        self.loose = models.ImplantType.objects.create(isotope=models.I125, name="loose seed", seeds_per_implant=1)
        rapid = models.ImplantType.objects.create(isotope=models.I125, name="rapidstrand", seeds_per_implant=10)

    def test_clean_number_fail(self):
        f = forms.ShipmentItemForm(data={"number": 0, "implant_type": self.loose.pk})
        f.is_valid()
        self.assertIn("number", f._errors)

    def test_clean_number_pass(self):
        f = forms.ShipmentItemForm(data={"number": 1, "implant_type": self.loose.pk})
        f.is_valid()
        self.assertNotIn("number", f._errors)


class TestShipmentItemFormSet(TestCase):
    def setUp(self):
        self.user = User.objects.create(username="user", password="password")
        self.loose = models.ImplantType.objects.create(isotope=models.I125, name="loose seed", seeds_per_implant=1)
        self.loose_pd = models.ImplantType.objects.create(isotope=models.PD103, name="loose seed", seeds_per_implant=1)
        self.rapid = models.ImplantType.objects.create(isotope=models.I125, name="rapidstrand", seeds_per_implant=10)

    def test_multiple_implants_of_same_type(self):
        fs = forms.ShipmentItemFormset(data={
            'form-TOTAL_FORMS': 2,
            'form-INITIAL_FORMS': 0,
            'form-0-implant_type': self.loose.pk,
            'form-0-number': 1,
            'form-1-implant_type': self.loose.pk,
            'form-1-number': 1,
        })

        with self.assertRaises(ValidationError):
            fs.is_valid()
            fs.clean()

    def test_multiple_isotope_fail(self):
        fs = forms.ShipmentItemFormset(data={
            'form-TOTAL_FORMS': 2,
            'form-INITIAL_FORMS': 0,
            'form-0-implant_type': self.loose.pk,
            'form-0-number': 1,
            'form-1-implant_type': self.loose_pd.pk,
            'form-1-number': 1,
        })

        with self.assertRaises(ValidationError):
            fs.is_valid()
            fs.clean()

    def test_no_items(self):
        fs = forms.ShipmentItemFormset(data={
            'form-TOTAL_FORMS': 0,
            'form-INITIAL_FORMS': 0,
        })

        with self.assertRaises(ValidationError):
            fs.is_valid()
            fs.clean()
