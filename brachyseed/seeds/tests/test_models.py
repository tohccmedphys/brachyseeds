import datetime
import math
from django.contrib.auth.models import User
from django.test import TestCase
from .. import models


class TestHelpers(TestCase):

    def test_mci2mbq(self):
        self.assertAlmostEqual(models.mCi2MBq(0.12345), 4.56765)

    def test_mbq2mci(self):
        self.assertAlmostEqual(models.MBq2mCi(37), 1)

    def test_i125(self):
        self.assertAlmostEqual(models.ISOTOPES[models.I125]["half_life"], 59.4)
        self.assertAlmostEqual(models.ISOTOPES[models.I125]["air_kerma_conversion_factor"], 1.270/37.)

    def test_pd103(self):
        self.assertAlmostEqual(models.ISOTOPES[models.PD103]["half_life"], 16.991)
        self.assertAlmostEqual(models.ISOTOPES[models.PD103]["air_kerma_conversion_factor"], 1.293/37.)


class TestShipment(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="user",password="password")

        self.received_days_ago = 4
        self.reference_date = datetime.date.today() - datetime.timedelta(days=self.received_days_ago)
        self.number_loose = 7
        self.seeds_per_loose = 1
        self.number_rapid = 4
        self.seeds_per_rapid = 11
        self.initial_activity = models.mCi2MBq(0.487)


        loose = models.ImplantType.objects.create(isotope=models.I125, name="loose seed", seeds_per_implant=self.seeds_per_loose)
        rapid = models.ImplantType.objects.create(isotope=models.I125, name="rapidstrand", seeds_per_implant=self.seeds_per_rapid)

        self.shipment = models.Shipment(
            po_number="12345",
            received=self.reference_date,
            reference_date=self.reference_date,
            received_by=self.user,
            activity_per_seed=self.initial_activity,
            surface=1.,
            dose_rate_1m=1.,
        )
        self.shipment.save()

        self.loose_item = models.ShipmentItem(
            shipment=self.shipment,
            implant_type=loose,
            number=self.number_loose
        )
        self.rapid_item = models.ShipmentItem(
            shipment=self.shipment,
            implant_type=rapid,
            number=self.number_rapid
        )
        self.shipment_items = [self.loose_item, self.rapid_item]

        models.ShipmentItem.objects.bulk_create(self.shipment_items)

    def test_cur_activity_per_seed(self):
        t12 = models.ISOTOPES[models.I125]["half_life"]
        decay_const = models.LN2 / t12
        cur = self.initial_activity * math.exp(-decay_const * self.received_days_ago)
        self.assertAlmostEqual(cur, self.shipment.cur_activity_per_seed())

    def test_num_seeds(self):
        num = self.number_loose * self.seeds_per_loose + self.number_rapid * self.seeds_per_rapid
        self.assertEqual(num, self.shipment.number_of_seeds())

    def test_activity_received(self):
        num = self.number_loose * self.seeds_per_loose + self.number_rapid * self.seeds_per_rapid
        self.assertAlmostEqual(num*self.initial_activity, self.shipment.activity_received())

    def test_activity_per_seed_mci(self):
        self.assertAlmostEqual(models.MBq2mCi(self.initial_activity), self.shipment.activity_per_seed_mci())

    def test_no_cals(self):
        self.assertIsNone(self.shipment.latest_calibration())

    def test_dose_rate_1m_exceeded(self):
        self.shipment.dose_rate_1m = 99999999999999
        self.assertTrue(self.shipment.dose_rate_1m_exceeded())

    def test_surface_exceeded(self):
        self.shipment.surface= 99999999999999
        self.assertTrue(self.shipment.surface_exceeded())

    def test_latest_cal(self):
        c1 = models.Calibration.objects.create(
            shipment=self.shipment,
            batch_number="1234",
            measured=datetime.date.today(),
            measured_by=self.user,
            well_reading=1.,
            well_reading_ref=1.,
        )
        c2 = models.Calibration.objects.create(
            shipment=self.shipment,
            batch_number="1234",
            measured=self.reference_date,
            measured_by=self.user,
            well_reading=1.,
            well_reading_ref=1.,
        )

        self.assertEqual(self.shipment.latest_calibration(), c1)

    def test_shipment_item_total_seeds(self):
        self.assertEqual(self.number_rapid*self.seeds_per_rapid, self.rapid_item.total_seeds())

    def test_shipment_item_orig_activity_per_seed(self):
        self.assertEqual(self.initial_activity, self.rapid_item.orig_activity_per_seed())

    def test_shipment_item_orig_activity_total(self):
        act = self.initial_activity*self.number_rapid*self.seeds_per_rapid
        self.assertEqual(act, self.rapid_item.orig_activity_total())

    def test_shipment_item_cur_activity_per_seed(self):
        t12 = models.ISOTOPES[models.I125]["half_life"]
        decay_const = models.LN2 / t12
        cur = self.initial_activity * math.exp(-decay_const * self.received_days_ago)
        self.assertAlmostEqual(cur, self.rapid_item.cur_activity_per_seed())

    def test_shipment_item_cur_activity_total(self):
        t12 = models.ISOTOPES[models.I125]["half_life"]
        decay_const = models.LN2 / t12
        cur = self.number_rapid*self.seeds_per_rapid*self.initial_activity * math.exp(-decay_const * self.received_days_ago)
        self.assertAlmostEqual(cur, self.rapid_item.cur_activity_total())


class TestStorageVial(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="user",password="password")

        self.received_days_ago = 4
        self.reference_date = datetime.date.today() - datetime.timedelta(days=self.received_days_ago)
        self.number_loose = 7
        self.seeds_per_loose = 1
        self.number_rapid = 4
        self.seeds_per_rapid = 11
        self.initial_activity = models.mCi2MBq(0.487)


        loose = models.ImplantType.objects.create(isotope=models.I125, name="loose seed", seeds_per_implant=self.seeds_per_loose)
        rapid = models.ImplantType.objects.create(isotope=models.I125, name="rapidstrand", seeds_per_implant=self.seeds_per_rapid)

        self.shipment = models.Shipment(
            po_number="12345",
            received=self.reference_date,
            reference_date=self.reference_date,
            received_by=self.user,
            activity_per_seed=self.initial_activity,
            surface=1,
            background=1,
            dose_rate_1m=1,
        )
        self.shipment.save()

        self.loose_item = models.ShipmentItem(
            shipment=self.shipment,
            implant_type=loose,
            number=self.number_loose
        )
        self.rapid_item = models.ShipmentItem(
            shipment=self.shipment,
            implant_type=rapid,
            number=self.number_rapid
        )
        self.shipment_items = [self.loose_item, self.rapid_item]

        models.ShipmentItem.objects.bulk_create(self.shipment_items)

        self.vial1 = models.StorageVial(vial_id="vial1")
        self.vial1.save()

        self.vial2 = models.StorageVial(vial_id="vial2")
        self.vial2.save()

        self.number_implanted=50
        self.number_stored = 5

        self.patient_implant = models.PatientImplant(
            shipment=self.shipment,
            patient_id="123456",
            implanted=datetime.date.today(),
            number_seeds=self.number_implanted,
            radonc=models.RadOnc.objects.create(name="rad onc")
        )
        self.patient_implant.save()

        svs = models.StorageVialSeeds(
            shipment=self.shipment,
            patient_implant=self.patient_implant,
            number_seeds=self.number_stored,
            date_stored=datetime.date.today(),
            storage_vial=self.vial1,
        )
        svs.save()

    def test_cur_activity(self):
        cur_activity = self.number_stored*self.loose_item.cur_activity_per_seed()
        self.assertEqual(cur_activity, self.vial1.cur_activity())

    def test_cur_number_seeds(self):
        self.assertEqual(self.number_stored, self.vial1.cur_number_seeds())

    def test_empty_date_set(self):
        self.vial1.empty()
        self.assertEqual(datetime.date.today(), self.vial1.last_emptied)

    def test_empty_count_0(self):
        self.vial1.empty()
        self.assertEqual(0, self.vial1.cur_number_seeds())


    def test_cur_stats(self):
        stats = models.StorageVialSeeds.objects.current_seed_stats()
        cur_activity = self.number_stored*self.loose_item.cur_activity_per_seed()
        self.assertDictEqual({
                'total_seeds':self.number_stored,
                'current_activity':cur_activity
            },
            stats
        )



