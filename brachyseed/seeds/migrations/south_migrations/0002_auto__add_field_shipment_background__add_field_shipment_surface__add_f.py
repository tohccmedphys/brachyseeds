# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Shipment.background'
        db.add_column(u'seeds_shipment', 'background',
                      self.gf('django.db.models.fields.FloatField')(default=1.0),
                      keep_default=False)

        # Adding field 'Shipment.surface'
        db.add_column(u'seeds_shipment', 'surface',
                      self.gf('django.db.models.fields.FloatField')(default=0),
                      keep_default=False)

        # Adding field 'Shipment.dose_rate_1m'
        db.add_column(u'seeds_shipment', 'dose_rate_1m',
                      self.gf('django.db.models.fields.FloatField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Shipment.background'
        db.delete_column(u'seeds_shipment', 'background')

        # Deleting field 'Shipment.surface'
        db.delete_column(u'seeds_shipment', 'surface')

        # Deleting field 'Shipment.dose_rate_1m'
        db.delete_column(u'seeds_shipment', 'dose_rate_1m')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'seeds.calibration': {
            'Meta': {'object_name': 'Calibration'},
            'batch_number': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'measured': ('django.db.models.fields.DateField', [], {}),
            'measured_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'calibrationmeasure_user'", 'to': u"orm['auth.User']"}),
            'reviewed': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'reviewed_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'calibrationreview_user'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.Shipment']"})
        },
        u'seeds.calibrationmeasurement': {
            'Meta': {'object_name': 'CalibrationMeasurement'},
            'calibration': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.Calibration']"}),
            'error': ('django.db.models.fields.FloatField', [], {}),
            'expected_kerma': ('django.db.models.fields.FloatField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'measured_kerma': ('django.db.models.fields.FloatField', [], {}),
            'number_seeds': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reading': ('django.db.models.fields.FloatField', [], {})
        },
        u'seeds.implanttype': {
            'Meta': {'unique_together': "(('isotope', 'name'),)", 'object_name': 'ImplantType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'isotope': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'seeds_per_implant': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'seeds.patientimplant': {
            'Meta': {'object_name': 'PatientImplant'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'implanted': ('django.db.models.fields.DateField', [], {}),
            'number_seeds': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'patient_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'radonc': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.RadOnc']"}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.Shipment']"})
        },
        u'seeds.radonc': {
            'Meta': {'object_name': 'RadOnc'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'seeds.shipment': {
            'Meta': {'ordering': "['-received']", 'object_name': 'Shipment'},
            'activity_per_seed': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'background': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'dose_rate_1m': ('django.db.models.fields.FloatField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'po_number': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'received': ('django.db.models.fields.DateField', [], {}),
            'received_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'reference_date': ('django.db.models.fields.DateField', [], {}),
            'surface': ('django.db.models.fields.FloatField', [], {})
        },
        u'seeds.shipmentitem': {
            'Meta': {'unique_together': "(('shipment', 'implant_type'),)", 'object_name': 'ShipmentItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'implant_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.ImplantType']"}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.Shipment']"})
        },
        u'seeds.storagevial': {
            'Meta': {'object_name': 'StorageVial'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_emptied': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'vial_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'seeds.storagevialseeds': {
            'Meta': {'object_name': 'StorageVialSeeds'},
            'date_stored': ('django.db.models.fields.DateField', [], {}),
            'disposed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number_seeds': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'patient_implant': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.PatientImplant']"}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.Shipment']"}),
            'storage_vial': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.StorageVial']"})
        }
    }

    complete_apps = ['seeds']