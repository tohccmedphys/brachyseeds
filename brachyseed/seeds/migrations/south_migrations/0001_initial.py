# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Shipment'
        db.create_table(u'seeds_shipment', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('po_number', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('received', self.gf('django.db.models.fields.DateField')()),
            ('received_by', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('reference_date', self.gf('django.db.models.fields.DateField')()),
            ('activity_per_seed', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'seeds', ['Shipment'])

        # Adding model 'ImplantType'
        db.create_table(u'seeds_implanttype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('isotope', self.gf('django.db.models.fields.CharField')(max_length=5)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('seeds_per_implant', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'seeds', ['ImplantType'])

        # Adding unique constraint on 'ImplantType', fields ['isotope', 'name']
        db.create_unique(u'seeds_implanttype', ['isotope', 'name'])

        # Adding model 'ShipmentItem'
        db.create_table(u'seeds_shipmentitem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shipment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['seeds.Shipment'])),
            ('implant_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['seeds.ImplantType'])),
            ('number', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal(u'seeds', ['ShipmentItem'])

        # Adding unique constraint on 'ShipmentItem', fields ['shipment', 'implant_type']
        db.create_unique(u'seeds_shipmentitem', ['shipment_id', 'implant_type_id'])

        # Adding model 'Calibration'
        db.create_table(u'seeds_calibration', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shipment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['seeds.Shipment'])),
            ('batch_number', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('measured', self.gf('django.db.models.fields.DateField')()),
            ('measured_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name='calibrationmeasure_user', to=orm['auth.User'])),
            ('reviewed', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('reviewed_by', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='calibrationreview_user', null=True, to=orm['auth.User'])),
        ))
        db.send_create_signal(u'seeds', ['Calibration'])

        # Adding model 'CalibrationMeasurement'
        db.create_table(u'seeds_calibrationmeasurement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('calibration', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['seeds.Calibration'])),
            ('number_seeds', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('reading', self.gf('django.db.models.fields.FloatField')()),
            ('measured_kerma', self.gf('django.db.models.fields.FloatField')()),
            ('expected_kerma', self.gf('django.db.models.fields.FloatField')()),
            ('error', self.gf('django.db.models.fields.FloatField')()),
        ))
        db.send_create_signal(u'seeds', ['CalibrationMeasurement'])

        # Adding model 'StorageVial'
        db.create_table(u'seeds_storagevial', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vial_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
            ('last_emptied', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'seeds', ['StorageVial'])

        # Adding model 'RadOnc'
        db.create_table(u'seeds_radonc', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=255)),
        ))
        db.send_create_signal(u'seeds', ['RadOnc'])

        # Adding model 'PatientImplant'
        db.create_table(u'seeds_patientimplant', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shipment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['seeds.Shipment'])),
            ('patient_id', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('implanted', self.gf('django.db.models.fields.DateField')()),
            ('number_seeds', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('radonc', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['seeds.RadOnc'])),
        ))
        db.send_create_signal(u'seeds', ['PatientImplant'])

        # Adding model 'StorageVialSeeds'
        db.create_table(u'seeds_storagevialseeds', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('shipment', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['seeds.Shipment'])),
            ('patient_implant', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['seeds.PatientImplant'])),
            ('number_seeds', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('date_stored', self.gf('django.db.models.fields.DateField')()),
            ('storage_vial', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['seeds.StorageVial'])),
            ('disposed', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'seeds', ['StorageVialSeeds'])


    def backwards(self, orm):
        # Removing unique constraint on 'ShipmentItem', fields ['shipment', 'implant_type']
        db.delete_unique(u'seeds_shipmentitem', ['shipment_id', 'implant_type_id'])

        # Removing unique constraint on 'ImplantType', fields ['isotope', 'name']
        db.delete_unique(u'seeds_implanttype', ['isotope', 'name'])

        # Deleting model 'Shipment'
        db.delete_table(u'seeds_shipment')

        # Deleting model 'ImplantType'
        db.delete_table(u'seeds_implanttype')

        # Deleting model 'ShipmentItem'
        db.delete_table(u'seeds_shipmentitem')

        # Deleting model 'Calibration'
        db.delete_table(u'seeds_calibration')

        # Deleting model 'CalibrationMeasurement'
        db.delete_table(u'seeds_calibrationmeasurement')

        # Deleting model 'StorageVial'
        db.delete_table(u'seeds_storagevial')

        # Deleting model 'RadOnc'
        db.delete_table(u'seeds_radonc')

        # Deleting model 'PatientImplant'
        db.delete_table(u'seeds_patientimplant')

        # Deleting model 'StorageVialSeeds'
        db.delete_table(u'seeds_storagevialseeds')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'seeds.calibration': {
            'Meta': {'object_name': 'Calibration'},
            'batch_number': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'measured': ('django.db.models.fields.DateField', [], {}),
            'measured_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'calibrationmeasure_user'", 'to': u"orm['auth.User']"}),
            'reviewed': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'reviewed_by': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'calibrationreview_user'", 'null': 'True', 'to': u"orm['auth.User']"}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.Shipment']"})
        },
        u'seeds.calibrationmeasurement': {
            'Meta': {'object_name': 'CalibrationMeasurement'},
            'calibration': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.Calibration']"}),
            'error': ('django.db.models.fields.FloatField', [], {}),
            'expected_kerma': ('django.db.models.fields.FloatField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'measured_kerma': ('django.db.models.fields.FloatField', [], {}),
            'number_seeds': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'reading': ('django.db.models.fields.FloatField', [], {})
        },
        u'seeds.implanttype': {
            'Meta': {'unique_together': "(('isotope', 'name'),)", 'object_name': 'ImplantType'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'isotope': ('django.db.models.fields.CharField', [], {'max_length': '5'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'seeds_per_implant': ('django.db.models.fields.PositiveIntegerField', [], {})
        },
        u'seeds.patientimplant': {
            'Meta': {'object_name': 'PatientImplant'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'implanted': ('django.db.models.fields.DateField', [], {}),
            'number_seeds': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'patient_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'radonc': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.RadOnc']"}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.Shipment']"})
        },
        u'seeds.radonc': {
            'Meta': {'object_name': 'RadOnc'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'seeds.shipment': {
            'Meta': {'ordering': "['-received']", 'object_name': 'Shipment'},
            'activity_per_seed': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'po_number': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'received': ('django.db.models.fields.DateField', [], {}),
            'received_by': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'reference_date': ('django.db.models.fields.DateField', [], {})
        },
        u'seeds.shipmentitem': {
            'Meta': {'unique_together': "(('shipment', 'implant_type'),)", 'object_name': 'ShipmentItem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'implant_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.ImplantType']"}),
            'number': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.Shipment']"})
        },
        u'seeds.storagevial': {
            'Meta': {'object_name': 'StorageVial'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_emptied': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'vial_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'seeds.storagevialseeds': {
            'Meta': {'object_name': 'StorageVialSeeds'},
            'date_stored': ('django.db.models.fields.DateField', [], {}),
            'disposed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'number_seeds': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'patient_implant': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.PatientImplant']"}),
            'shipment': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.Shipment']"}),
            'storage_vial': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['seeds.StorageVial']"})
        }
    }

    complete_apps = ['seeds']