# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-05 21:01
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Calibration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('batch_number', models.CharField(blank=True, max_length=255, null=True)),
                ('well_reading', models.FloatField(verbose_name=b'Well counter check reading')),
                ('well_reading_ref', models.FloatField(verbose_name=b'Well counter check reference')),
                ('measured', models.DateField(verbose_name=b'Date Measured')),
                ('reviewed', models.DateField(blank=True, null=True, verbose_name=b'Date Reviewed')),
                ('measured_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='calibrationmeasure_user', to=settings.AUTH_USER_MODEL)),
                ('reviewed_by', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='calibrationreview_user', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='CalibrationMeasurement',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number_seeds', models.PositiveIntegerField(verbose_name=b'Number of Seeds Measured')),
                ('reading', models.FloatField(verbose_name=b'Calibrator Reading (MBq)')),
                ('measured_kerma', models.FloatField(verbose_name=b'Measured Air Kerma Per Seed (Gy m2 h^-1)')),
                ('expected_kerma', models.FloatField(verbose_name=b'Expected Air Kerma Per Seed (Gy m2 h^-1)')),
                ('error', models.FloatField(verbose_name=b'% Error')),
                ('calibration', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='seeds.Calibration')),
            ],
        ),
        migrations.CreateModel(
            name='ImplantType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('isotope', models.CharField(choices=[(b'I125', b'I125'), (b'Pd103', b'Pd103')], max_length=5)),
                ('name', models.CharField(max_length=255)),
                ('seeds_per_implant', models.PositiveIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='PatientImplant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('patient_id', models.CharField(max_length=255)),
                ('implanted', models.DateField(verbose_name=b'Patient Implant Date')),
                ('number_seeds', models.PositiveIntegerField(verbose_name=b'Total Number of Seeds Implanted')),
            ],
        ),
        migrations.CreateModel(
            name='RadOnc',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Shipment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('po_number', models.CharField(max_length=255)),
                ('received', models.DateField(verbose_name=b'Date Shipment Received')),
                ('reference_date', models.DateField(verbose_name=b'Activity Reference Date')),
                ('activity_per_seed', models.FloatField(blank=True, null=True, verbose_name=b'Activity Per Seed (MBq)')),
                ('background', models.FloatField(default=1.0, verbose_name=b'Background reading (&micro;R/h)')),
                ('surface', models.FloatField(verbose_name=b'Surface reading (&micro;R/h)')),
                ('dose_rate_1m', models.FloatField(verbose_name=b'Dose rate at 1m (&micro;R/h)')),
                ('received_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-received'],
            },
        ),
        migrations.CreateModel(
            name='ShipmentItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.PositiveIntegerField(help_text=b'Number of this seed type received')),
                ('implant_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='seeds.ImplantType')),
                ('shipment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='seeds.Shipment')),
            ],
        ),
        migrations.CreateModel(
            name='StorageVial',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vial_id', models.CharField(max_length=255, unique=True, verbose_name=b'Unique Storage Vial ID')),
                ('last_emptied', models.DateField(blank=True, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='StorageVialSeeds',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number_seeds', models.PositiveIntegerField(help_text=b'Total number of seeds stored')),
                ('date_stored', models.DateField()),
                ('disposed', models.BooleanField(default=False)),
                ('patient_implant', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='seeds.PatientImplant')),
                ('shipment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='seeds.Shipment')),
                ('storage_vial', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='seeds.StorageVial')),
            ],
            options={
                'verbose_name_plural': 'Storage Vial Seeds',
            },
        ),
        migrations.AddField(
            model_name='patientimplant',
            name='radonc',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='seeds.RadOnc', verbose_name=b'Radiation Oncologist'),
        ),
        migrations.AddField(
            model_name='patientimplant',
            name='shipment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='seeds.Shipment'),
        ),
        migrations.AlterUniqueTogether(
            name='implanttype',
            unique_together=set([('isotope', 'name')]),
        ),
        migrations.AddField(
            model_name='calibration',
            name='shipment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='seeds.Shipment'),
        ),
        migrations.AlterUniqueTogether(
            name='shipmentitem',
            unique_together=set([('shipment', 'implant_type')]),
        ),
    ]
