
import calendar
import collections
import datetime
import json
import math
import time

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.models import User, Group
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.urls import reverse, resolve
from django.db import transaction
from django.db import models
from django.forms import model_to_dict
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.template.loader import get_template
from django.utils import formats, timezone
from django.utils.translation import gettext as _
from django.views.generic import CreateView, DetailView, ListView, TemplateView, View
from listable.views import BaseListableView, DATE_RANGE, SELECT_MULTI, TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LAST_MONTH, THIS_MONTH, THIS_YEAR

from seeds import forms
from seeds import models as s_models

JSON_DATE_FORMAT = settings.BACKEND_DATE_DATA_FORMAT


def default_json(obj):
    """Default JSON serializer."""
    if isinstance(obj, datetime.date):
        return obj.strftime(JSON_DATE_FORMAT)
    return obj


def current_well_reading_ref():
    """Return current expected reading of CS137 well references"""

    reading_data = timezone.datetime.strptime(settings.WELL_READING_DATE, settings.PYTHON_DATE_FORMAT).date()
    days = (timezone.now().date() - reading_data).days
    decay_const = s_models.CS137_DECAY_CONST
    return settings.WELL_READING_REF * math.exp(-decay_const * days)


class StorageContentMixin(object):

    def get_context_data(self, *args, **kwargs):
        context = super(StorageContentMixin, self).get_context_data(*args, **kwargs)
        stored = s_models.StorageVialSeeds.objects.current_seed_stats()
        context["stored"] = stored
        return context


class ShipmentList(LoginRequiredMixin, BaseListableView):

    model = s_models.Shipment
    paginate_by = 50
    order_by = ['-received']

    kwarg_filters = None

    fields = (
        'pk',
        'po_number',
        'received',
        'received_by__username',
        'activity_received',
        'calibrated',
        'calibrated_by',
    )

    headers = {
        'pk': _('ID'),
        'po_number': _('PO #'),
        'received': _('Received'),
        'received_by__username': _('Received By'),
        'activity_received': _('Activity Received (MBq)'),
        'calibrated': _('Calibrated'),
        'calibrated_by': _('Calibrated By'),
    }

    widgets = {
        'received': DATE_RANGE,
        'received_by__username': SELECT_MULTI,
        # 'calibrated': DATE_RANGE,
        # 'calibrated_by': SELECT_MULTI
    }

    date_ranges = {
        'received': [TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LAST_MONTH, THIS_MONTH, THIS_YEAR],
        # 'calibrated': [TODAY, YESTERDAY, LAST_WEEK, THIS_WEEK, LAST_MONTH, THIS_MONTH, THIS_YEAR],
    }

    search_fields = {
        'calibrated': False,
        'activity_received': False,
        'calibrated_by': False,
    }

    order_fields = {
        'calibrated': False,
        'calibrated_by': False,
        'activity_received': False,
    }

    def __init__(self, *args, **kwargs):
        super(ShipmentList, self).__init__(*args, **kwargs)
        self.templates = {
            'shipment_detail': get_template('seeds/table_context_po_number.html'),
        }

    def get_page_title(self, f=None):
        # if not f:
        #     return 'All Service Events'
        # to_return = 'Service Events'
        # filters = f.split('_')
        # for filt in filters:
        #     [key, val] = filt.split('-')
        #     if key == 'ss':
        #         to_return = to_return + ' - Status: ' + s_models.ServiceEventStatus.objects.get(pk=val).name
        #     elif key == 'ar':
        #         to_return = to_return + ' - Approval is ' + ((not bool(int(val))) * 'not ') + 'required'
        #
        # return to_return
        return 'All Shipments'

    def get(self, request, *args, **kwargs):
        if self.kwarg_filters is None:
            self.kwarg_filters = kwargs.pop('f', None)
        return super(ShipmentList, self).get(request, *args, **kwargs)

    def get_context_data(self, *args, **kwargs):
        context = super(ShipmentList, self).get_context_data(*args, **kwargs)
        current_url = resolve(self.request.path_info).url_name
        context['view_name'] = current_url
        f = self.request.GET.get('f', False)
        context['kwargs'] = {'f': f} if f else {}
        context['page_title'] = self.get_page_title(f)
        return context

    def get_queryset(self):
        qs = super(ShipmentList, self).get_queryset()

        # if self.kwarg_filters is None:
        #     self.kwarg_filters = self.request.GET.get('f', None)
        #
        # if self.kwarg_filters is not None:
        #     filters = self.kwarg_filters.split('_')
        #     for filt in filters:
        #         [key, val] = filt.split('-')
        #         if key == 'ss':
        #             qs = qs.filter(service_status=val)
        #         elif key == 'ar':
        #             qs = qs.filter(is_approval_required=bool(int(val)))
        #         elif key == 'ss.ar':
        #             qs = qs.filter(service_status__is_approval_required=bool(int(val)))
        return qs

    def calibrated(self, shipment):
        try:
            return shipment.latest_calibration().measured.strftime('%d %b %Y')
        except AttributeError:
            return None

    def calibrated_by(self, shipment):
        try:
            return shipment.latest_calibration().measured_by.username
        except AttributeError:
            return None

    def activity_received(self, shipment):
        return '%.2f' % shipment.activity_received()

    def po_number(self, shipment):
        template = self.templates['shipment_detail']
        c = {'shipment': shipment, 'request': self.request}
        return template.render(c)


class Shipment(LoginRequiredMixin, CreateView):

    form_class = forms.ShipmentForm
    model = s_models.Shipment

    def form_valid(self, form):
        context = self.get_context_data()
        formset = context["formset"]
        if not formset.is_valid():

            messages.add_message(
                self.request,
                messages.constants.ERROR,
                "Please correct the errors in red below and submit again.",
                extra_tags="danger",
            )
            context["form"] = form
            return self.render_to_response(context)

        super(Shipment, self).form_valid(form)

        for f in formset:
            if f.changed_data:
                f.instance.shipment = self.object
                f.save()

        if self.object.surface > settings.SURFACE_READING_LIMIT:
            messages.warning(self.request, "Surface reading exceeds tolerance! Contact RSO & GE Health Canada")

        if self.object.dose_rate_1m > settings.DOSE_RATE_1M_LIMIT:
            messages.warning(self.request, "Dose rate at 1m exceeds tolerance! Contact RSO & GE Health Canada")

        messages.success(self.request, "Successfully logged shipment %s " % self.object.pk)
        return HttpResponseRedirect(reverse("shipment-detail", kwargs={"pk": self.object.pk}))

    def get_context_data(self, *args, **kwargs):
        context = super(Shipment, self).get_context_data(*args, **kwargs)

        if self.request.POST:
            context["formset"] = forms.ShipmentItemFormset(self.request.POST)
        else:
            context["formset"] = forms.ShipmentItemFormset(queryset=s_models.Shipment.objects.none())

        seed_types = [model_to_dict(i) for i in s_models.ImplantType.objects.all()]

        context["seed_types"] = json.dumps(seed_types, default=default_json)
        context["surface_limit"] = json.dumps(settings.SURFACE_READING_LIMIT)
        context["dose_rate_1m_limit"] = json.dumps(settings.DOSE_RATE_1M_LIMIT)
        return context

    def get_success_url(self):
        return reverse("shipment-detail", kwargs={"pk": self.object.pk})


class ShipmentDetail(LoginRequiredMixin, StorageContentMixin, DetailView):

    model = s_models.Shipment

    def post(self, *args, **kwargs):
        shipment = self.get_object()
        data = json.loads(self.request.POST.get('data'))

        print(data)

        try:
            if data.get("form_type") == "calibration":
                return self.handle_calibration(shipment, data)
            elif data.get("form_type") == "patient_implant":
                return self.handle_implant(shipment, data)
            elif data.get("form_type") == "review_calibration":
                return self.handle_review(shipment, data)

            return JsonResponse('No form type included', status=403, safe=False)
        except Exception as e:
            return JsonResponse(str(e), status=403, safe=False)

    def handle_implant(self, shipment, data):

        implant_date = datetime.datetime.strptime(data["implanted"], JSON_DATE_FORMAT).date()

        with transaction.atomic():

            implant = s_models.PatientImplant(
                patient_id=data.get("patient_id"),
                implanted=implant_date,
                shipment=shipment,
                number_seeds=int(data.get("number_seeds")),
                radonc_id=data.get("radonc"),
            )

            implant.save()

            nstored = data.get("number_seeds_stored") or 0
            if nstored > 0:
                storage = s_models.StorageVialSeeds(
                    shipment=shipment,
                    patient_implant_id=implant.pk,
                    number_seeds=nstored,
                    date_stored=implant_date,
                    storage_vial_id=data.get("storage_vial"),
                )
                storage.save()

            response_data = {"success": True, "implant_id": implant.pk}
            if nstored > 0:
                response_data["storage_id"] = storage.pk

        return JsonResponse(response_data)

    def handle_calibration(self, shipment, data):

        cal_date = datetime.datetime.strptime(data["date"], JSON_DATE_FORMAT).date()

        with transaction.atomic():

            calibration = s_models.Calibration.objects.create(
                batch_number=data.get("batch_number", ""),
                shipment=shipment,
                measured=cal_date,
                measured_by=self.request.user,
                well_reading=data.get("well_reading"),
                well_reading_ref=data.get("well_reading_ref")
            )

            to_create = []
            for md in data["measurements"]:

                m = s_models.CalibrationMeasurement(
                    calibration=calibration,
                    number_seeds=md["number_seeds"],
                    reading=md["reading"],
                    measured_kerma=md["measured_kerma"],
                    expected_kerma=md["expected_kerma"],
                    error=md["error"]
                )
                to_create.append(m)

            s_models.CalibrationMeasurement.objects.bulk_create(to_create)

            response_data = {"success": True, "id": calibration.pk}

        return JsonResponse(response_data)

    def handle_review(self, shipment, data):

        try:
            user = s_models.User.objects.get(username=data['username'])
        except (s_models.User.DoesNotExist, KeyError):
            user = None

        if user is None:
            return JsonResponse("Invalid authorization username/password", status=401, safe=False)

        cal = s_models.Calibration.objects.get(pk=data['id'])
        cal.reviewed = datetime.date.today()
        cal.reviewed_by = user
        cal.save()

        data = {"reviewed_by": user.username}

        return JsonResponse(data)

    def get_context_data(self, *args, **kwargs):
        context = super(ShipmentDetail, self).get_context_data(*args, **kwargs)

        serial_calibrations = [c.model_to_dict() for c in self.object.calibration_set.prefetch_related('calibrationmeasurement_set').order_by("measured")]
        serial_implant_data = [i.model_to_dict() for i in self.object.patientimplant_set.prefetch_related("storagevialseeds_set")]

        context["shipment"] = self.object.model_to_dict()
        context["shipmentitems"] = [si.model_to_dict() for si in self.object.shipmentitem_set.all()]
        context["calibrations"] = serial_calibrations
        context["well_reading_ref"] = current_well_reading_ref()
        context["well_reading_ref_date"] = settings.WELL_READING_DATE
        context["well_reading_tol"] = settings.WELL_READING_TOL

        context["isotopes"] = json.dumps(s_models.ISOTOPES)
        context["implanttype"] = [it.model_to_dict() for it in s_models.ImplantType.objects.all()]
        context["radoncs"] = [ro.model_to_dict() for ro in s_models.RadOnc.objects.all()]
        context["storagevials"] = [sv.model_to_dict() for sv in s_models.StorageVial.objects.all()]
        context["implants"] = json.dumps(serial_implant_data, default=default_json)

        reviewers, _ = Group.objects.get_or_create(name=settings.REVIEW_GROUP_NAME)

        context["reviewers"] = [r.username for r in reviewers.user_set.all()]

        return context


def date_to_ms(d):
    return time.mktime(d.timetuple())*1000


class Stats(StorageContentMixin, TemplateView):

    template_name = "seeds/stats.html"

    def get_date_oldest(self):

        oldest_cal = s_models.Calibration.objects.order_by('measured').earliest('measured').measured
        oldest_imp = s_models.PatientImplant.objects.order_by('implanted').earliest('implanted').implanted

        return {
            'oldest_cal': {'month': oldest_cal.month, 'year': oldest_cal.year},
            'oldest_imp': {'month': oldest_imp.month, 'year': oldest_imp.year},
        }

    def get_context_data(self, *args, **kwargs):
        context = super(Stats, self).get_context_data(*args, **kwargs)
        context['vial_list'] = s_models.StorageVial.objects.all()
        context['date_ranges'] = self.get_date_oldest()
        return context


class GetStatsData(View):

    def get(self, *args, **kwargs):

        data = self.request.GET
        date_from = datetime.date(year=int(data.get('from_year')), month=int(data.get('from_month')), day=1)
        to_month = int(data.get('to_month'))
        to_year = int(data.get('to_year'))
        wd, nd = calendar.monthrange(to_year, to_month)
        date_to = datetime.date(year=to_year, month=to_month, day=nd)

        # implant data
        implants = s_models.PatientImplant.objects.filter(implanted__range=(date_from, date_to)).order_by('implanted')

        imp_stats = implants.aggregate(
            seeds_implanted=models.Sum('number_seeds'),
            seeds_per_patient=models.Avg('number_seeds')
        )

        imp_stats['seeds_per_patient'] = round(imp_stats['seeds_per_patient'] or 0, 2)
        imp_stats['patients_implanted'] = implants.count()

        cnt = collections.Counter()
        for pi in implants:
            cnt[(pi.implanted.year, pi.implanted.month)] += 1

        sorted_ = sorted(cnt.items())
        imp_stats["implant_ticks"] = {y: {} for ((y, m), n) in sorted_}
        for ((y, m), n) in sorted_:
            imp_stats["implant_ticks"][y][m] = n

        # calibration data
        calibrations = s_models.Calibration.objects.filter(measured__range=(date_from, date_to)).order_by('measured')

        well_readings = []
        well_reference = []
        avg_measurements = []
        avg_error = []
        calibration_measurements = []
        expected_measurement = []

        for c in calibrations:
            cms = c.calibrationmeasurement_set.all()
            if len(cms) > 0:
                x = date_to_ms(c.measured)
                well_readings.append({'x': x, 'y': c.well_reading})
                well_reference.append({'x': x, 'y': c.well_reading_ref})

                avg_measurements.append({'x': x, 'y': sum([cm.measured_kerma for cm in cms]) / len(cms)})
                avg_error.append({'x': x, 'y': sum([cm.error for cm in cms]) / len(cms)})
                for cm in cms:
                    calibration_measurements.append({'x': x, 'y': cm.measured_kerma})

                expected_measurement.append({'x': x, 'y': cms[0].expected_kerma})

        cal_err_stats = {
            'avg_measurements': {
                'name': 'avg_measurements',
                'values': avg_measurements,
                'legend': 'Average Meas. Air Kerma'
            },
            'avg_error': {
                'name': 'avg_error',
                'values': avg_error,
                'legend': 'Average Error'
            },
            'calibration_measurements': {
                'name': 'calibration_measurements',
                'values': calibration_measurements,
                'legend': 'Calibration Measurements'
            },
            'expected_measurement': {
                'name': 'expected_measurement',
                'values': expected_measurement,
                'legend': 'Expected Measurement'
            },
        }

        cal_reading_stats = [
            {
                'name': 'well_readings',
                'values': well_readings,
                'legend': 'Reading'
            },
            {
                'name': 'well_reference',
                'values': well_reference,
                'legend': 'Reference'
            }
        ]

        shipments = s_models.Shipment.objects.filter(received__range=(date_from, date_to))
        seeds_received = 0
        total_activity = 0
        for shipment in shipments:
            seeds_received += shipment.number_of_seeds()
            total_activity += shipment.activity_received()

        rec_stats = {
            'shipments': shipments.count(),
            'seeds_received': seeds_received,
            'total_activity': round(total_activity, 3)
        }

        cnt.clear()
        for s in shipments:
            cnt[(s.received.year, s.received.month)] += 1

        sorted_ = sorted(cnt.items())
        rec_stats["shipment_ticks"] = {y: {} for ((y, m), n) in sorted_}
        for ((y, m), n) in sorted_:
            rec_stats["shipment_ticks"][y][m] = n

        return JsonResponse({
            'implant_data': imp_stats,
            'cal_reading_data': cal_reading_stats,
            'cal_err_data': cal_err_stats,
            'received_data': rec_stats
        })


class StorageVial(DetailView):
    model = s_models.StorageVial


class EmptyStorage(DetailView):

    model = s_models.StorageVial

    def post(self, *args, **kwargs):

        vial = self.get_object()
        vial.empty()

        return JsonResponse({'message': "Successfully Emptied Vial {}".format(vial.pk), 'id': vial.pk, 'last_emptied': vial.last_emptied.strftime('%d/%m/%Y')})
