
from django.contrib.auth.models import User
from django.db.models import Q
from django.forms import ModelForm, ValidationError, FloatField
from django.forms.models import modelformset_factory
from django.utils import timezone

from seeds import models


class ShipmentForm(ModelForm):

    activity_per_seed_mci = FloatField(label="Activity/seed (mCi)")

    class Meta:
        model = models.Shipment
        fields = ("po_number", "received", "received_by", "reference_date", "activity_per_seed_mci", "background", "surface", "dose_rate_1m",)

    def __init__(self, *args, **kwargs):
        super(ShipmentForm, self).__init__(*args, **kwargs)

        for f in self.fields:
            self.fields[f].widget.attrs["class"] = "form-control"

        date_fields = ["received", "reference_date"]
        for f in date_fields:
            self.fields[f].widget.attrs["class"] += " datepicker date icon-in"
            # self.fields[f].widget.attrs["readonly"] = "readonly"
            self.fields[f].input_formats = ["%d-%m-%Y"]

        for f in ['received_by']:

            if self.instance.id and self.instance.received_by:
                self.fields[f].queryset = User.objects.filter(Q(is_active=True) | Q(id=self.instance.received_by.id))
            else:
                self.fields[f].queryset = User.objects.filter(is_active=True)
            self.fields[f].widget.attrs['class'] += ' select2'

    def clean(self):
        cleaned_data = super(ShipmentForm, self).clean()
        if "reference_date" in self.cleaned_data and "received" in self.cleaned_data:
            ref_date = cleaned_data["reference_date"]
            rec_date = cleaned_data["received"]
            #if ref_date > rec_date:
            #    raise ValidationError("Reference Date Can't be after Received Date")

            if rec_date > timezone.now().date():
                raise ValidationError("Received Date can't be in the future")

        return cleaned_data

    def save(self, commit=True):
        instance = super(ShipmentForm, self).save(commit=commit)
        instance.activity_per_seed = models.mCi2MBq(self.cleaned_data["activity_per_seed_mci"])
        if commit:
            instance.save()
        return instance


class ShipmentItemForm(ModelForm):

    class Meta:
        model = models.ShipmentItem
        fields = ("implant_type", "number",)

    def __init__(self, *args, **kwargs):
        super(ShipmentItemForm, self).__init__(*args, **kwargs)

        self.fields['implant_type'].widget.attrs['class'] = 'select2 ' + (self.prefix or '')
        self.fields['number'].widget.attrs['class'] = 'form-control ' + (self.prefix or '')

    def clean_number(self):
        n = self.cleaned_data["number"]
        if n < 1:
            raise ValidationError("Number must be greater than 1 (or leave both fields blank)!")
        return n


BaseShipmentItemFormset = modelformset_factory(models.ShipmentItem, form=ShipmentItemForm, extra=6)


class ShipmentItemFormset(BaseShipmentItemFormset):

    def clean(self):
        implant_types = [f.instance.implant_type for f in self.forms if f.instance and hasattr(f.instance, "implant_type")]
        if len(implant_types) != len(set(implant_types)):
            raise ValidationError("You can only add one entry for each Implant Type")

        isotopes = [it.isotope for it in implant_types]
        if len(set(isotopes)) > 1:
            msg = (
                "Shipment entries are currently limited to a single isotope."
                " Please enter different shipments for each isotope."
            )

            raise ValidationError(msg)

        for form in self.extra_forms:
            if form.has_changed():
                return

        raise ValidationError("You need to add at least one item before submiting!")


class CalibrationMeasurementForm(ModelForm):
    class Meta:
        model = models.CalibrationMeasurement
        fields = ("number_seeds", "reading", "measured_kerma", "expected_kerma", "error")


BaseCalibrationMeasurementFormset = modelformset_factory(models.CalibrationMeasurement, form=CalibrationMeasurementForm, extra=10)


class CalibrationMeasurementFormset(BaseCalibrationMeasurementFormset):
    pass
