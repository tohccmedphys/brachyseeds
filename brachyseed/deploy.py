from brachyseed import wsgi
import socket
from cherrypy import wsgiserver

if socket.gethostname() == "OHQATRACKDB01":
    sn = "qatrack.ottawahospital.on.ca"
else:
    sn = socket.gethostname()

server = wsgiserver.CherryPyWSGIServer(
    ("0.0.0.0", 8011), wsgi.application, server_name=sn,
)
server.start()
