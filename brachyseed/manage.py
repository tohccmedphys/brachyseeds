#!/usr/bin/env python
import os
import sys

sm =  "brachyseed.settings.local" if 'test' not in sys.argv else  "brachyseed.settings.test"
if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", sm)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
